page 51002 "ECPFichaVariosC01"
{
    Caption = 'Ficha de Varios';
    PageType = Card;
    SourceTable = ECPVariosC01;
    AdditionalSearchTerms = 'Curso 01';
    AboutTitle = 'Ficha de Tabla Varios';
    AboutText = 'Se usa para editar registros de una tabla con Codigos y Descripciones';

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                    AboutTitle = 'Tipo de registro';
                    AboutText = 'Indica si el registrp es de Vacunas, Otros, ...';
                }
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;

                }
                field(PeriodoVacunacion; Rec.PeriodoVacunacion)
                {
                    ToolTip = 'Indicar el periodo para calcular la fecha de la siguiente dosis.';
                    ApplicationArea = All;
                    Visible = Rec.Tipo = Rec.Tipo::Vacuna;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
            }
            group(Informacion)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                    Editable = false;
                }

            }
        }
    }
}
