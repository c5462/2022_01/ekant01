page 51010 "ECPEjemploDiccionarioC01"
{
    ApplicationArea = All;
    Caption = 'Ejemplo Diccionario';
    PageType = List;
    SourceTable = Integer;  // es una tabla virtual que no existe en SQL, pero esta cargada con numeros
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Num; Rec.Number)
                {
                    ApplicationArea = All;
                    Caption = 'Num';
                    //AutoFormatExpression='';
                }
                field(Campo1; ValorCeldaF(Rec.Number, 1))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 1';
                }
                field(Campo2; ValorCeldaF(Rec.Number, 2))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 2';
                }
                field(Campo3; ValorCeldaF(Rec.Number, 3))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 3';
                }
                field(Campo4; ValorCeldaF(Rec.Number, 4))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 4';
                }
                field(Campo5; ValorCeldaF(Rec.Number, 5))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 5';
                }
                field(Campo6; ValorCeldaF(Rec.Number, 6))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 6';
                }
                field(Campo7; ValorCeldaF(Rec.Number, 7))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 7';
                }
                field(Campo8; ValorCeldaF(Rec.Number, 8))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 8';
                }
                field(Campo9; ValorCeldaF(Rec.Number, 9))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 9';
                }
            }
        }
    }
    var
        xDic: Dictionary of [Integer, List of [Decimal]];

    local procedure ValorCeldaF(pFila: Integer; pCol: Integer): Integer
    begin
        exit(xDic.Get(pFila).Get(pCol));
    end;

    trigger OnOpenPage()
    var
        xFila: Integer;
        xCol: Integer;
        xlNum: List of [Decimal];
    begin
        for xFila := 1 to 15 do begin
            Clear(xlNum);
            for xCol := 1 to 9 do begin
                xlNum.Add(random(55555));
            end;
            xDic.Add(xFila, xlNum);
        end;
        rec.SetRange(Number, 1, 15);
        EjemploNotificacionF();
    end;

    local procedure EjemploNotificacionF()
    var
        xlNotif: Notification;
        i: Integer;
    begin
        for i := 1 to 3 do begin
            xlNotif.Id(CreateGuid());
            xlNotif.Message(StrSubstNo('Carga %1 Finalizada', i));
            xlNotif.Send();
        end;
        xlNotif.Id(CreateGuid());
        xlNotif.Message('El cliente no tiene vacunas');
        xlNotif.AddAction('Abrir lista clientes', Codeunit::ECPFuncionesAppC01, 'AbrirListaClientesF');
        xlNotif.SetData('CodigoCliente', '10000');
        xlNotif.Send();
    end;
}
