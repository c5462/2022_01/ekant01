/// <summary>
/// Page ECPListaPlanesVacunacionC01 (ID 51005).
/// </summary>
page 51005 "ECPListaPlanesVacunacionC01"
{
    ApplicationArea = All;
    Caption = 'Lista Planes Vacunacion';
    PageType = List;
    SourceTable = ECPPlanesVacunacionC01;
    UsageCategory = Lists;
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;
    Editable = false;
    CardPageId = ECPFichaCabPlanVacunacionC01;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoCab)
                {
                    ToolTip = 'Specifies the value of the Codigo field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    Caption = 'Empresa Vacunadora';
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVac; Rec.NombreEmpresaF(Rec.EmpresaVacunadora))
                {
                    Caption = 'Nombre Empresa Vacunadora';
                    ToolTip = 'Es el nombre del proveedor que hace la vacunacion.';
                    ApplicationArea = All;
                    Editable = false;
                }

                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
                field(FechaInicioVacunacionPlanif; Rec.FechaInicioVacunacionPlanif)
                {
                    Caption = 'Fecha Inicio Vacunacion';
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }

            }
        }
    }
    procedure GetSelectionFilterF(var rSalida: Record ECPPlanesVacunacionC01)
    begin
        CurrPage.SetSelectionFilter(rSalida);
    end;
}
