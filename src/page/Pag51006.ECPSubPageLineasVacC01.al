/// <summary>
/// Page SubFormulario Lineas de Vacunacion
/// </summary>
page 51006 "ECPSubPageLineasVacC01"
{
    Caption = 'Detalles de vacunacion';
    PageType = ListPart;
    SourceTable = ECPLinPlanVacunacionC01;
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(CodigoLin; Rec.CodigoLin)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                    Visible = false;
                    Editable = false;
                }
                field(Linea; Rec.Linea)
                {
                    ToolTip = 'Specifies the value of the Nº Linea field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(ClienteAvacunar; Rec.ClienteAvacunar)
                {
                    ToolTip = 'Specifies the value of the Cliente a Vacunar field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; rec.NombreClienteF())
                {
                    caption = 'Nombre Cliente';
                    ApplicationArea = All;
                }
                field(FechaVacunacion; Rec.FechaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha Vacunacion field.';
                    ApplicationArea = All;
                }
                field(TipoVacuna; Rec.CodigoVacuna)
                {
                    Caption = 'Tipo Vacuna';
                    ToolTip = 'Specifies the value of the Tipo Vacuna field.';
                    ApplicationArea = All;
                }
                field(NombreVacuna; rec.NombreOtrosF(eTipoVarios::Vacuna, Rec.CodigoVacuna))
                {
                    caption = 'Nombre Vacuna';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(FechaSigteDosis; Rec.FechaSigteDosis)
                {
                    ToolTip = 'Specifies the value of the Fecha prevista Sigte Dosis field.';
                    ApplicationArea = All;
                    //Editable = false;
                }
                field(TipoOtros; Rec.CodigoOtros)
                {
                    Caption = 'Tipo Otros';
                    ToolTip = 'Specifies the value of the Tipo Otros field.';
                    ApplicationArea = All;
                }
                field(NombreOtros; rec.NombreOtrosF(eTipoVarios::Otros, Rec.CodigoOtros))
                {
                    Caption = 'Nombre Otros';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                    Editable = false;
                }
            }
        }
    }

    var
        eTipoVarios: Enum ECPtipoC01;


    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        Rec.FechaVacunacion := Rec.CargarFecha(Rec.CodigoLin);
    end;
}
