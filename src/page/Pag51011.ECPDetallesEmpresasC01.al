page 51011 "ECPDetallesEmpresasC01"
{
    ApplicationArea = All;
    Caption = 'Lista de Empresas con Numero Registros';
    PageType = List;
    SourceTable = Company;
    UsageCategory = Lists;
    SaveValues = true;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Nombre; Rec.Name)
                {
                    ApplicationArea = All;
                    Caption = 'Nombre de Empresa';
                }
                field(NumClientes; DatosTablaF(rCustomer, false))
                {
                    ApplicationArea = All;
                    Caption = 'Numero de Clientes';
                    DrillDown = true;
                    trigger OnDrillDown()
                    begin
                        DatosTablaF(rCustomer, true);
                        //NumRegsF(Rec.Name, 18, true);
                    end;
                }
                field(NumProveedores; DatosTablaF(rVendor, false))
                {
                    ApplicationArea = All;
                    Caption = 'Numero de Proveedores';
                    DrillDown = true;
                    trigger OnDrillDown()
                    begin
                        DatosTablaF(rVendor, true);
                        //NumRegsF(23, true);
                    end;
                }
                field(NumCuentas; DatosTablaF(rGLAcount, false))
                {
                    ApplicationArea = All;
                    Caption = 'Numero de Cuentas';
                    DrillDown = true;
                    trigger OnDrillDown()
                    begin
                        DatosTablaF(rGLAcount, true);
                        //NumRegsF(15, true);
                    end;
                }
                field(NumProductos; DatosTablaF(rItem, false))
                {
                    ApplicationArea = All;
                    Caption = 'Numero de Productos';
                    DrillDown = true;
                    trigger OnDrillDown()
                    begin
                        DatosTablaF(rItem, true);
                        //NumRegsF(27, true);
                    end;
                }
                field(NumAgentes; DatosTablaF(rSalesPerson, false))
                {
                    ApplicationArea = All;
                    Caption = 'Numero de Agentes';
                    DrillDown = true;
                    trigger OnDrillDown()
                    begin
                        DatosTablaF(rSalesPerson, true);
                    end;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(CopiaSeguridad)
            {
                ApplicationArea = All;
                Caption = 'Copia de Seguridad';
                Image = Copy;
                trigger OnAction()
                begin
                    ExportarTodo();
                end;
            }
            action(RestaurarCopia)
            {
                ApplicationArea = All;
                Caption = 'Restaurar Copia de Fichero';
                Image = Copy;
                trigger OnAction()
                begin
                    ImportarDatosEmpresa();
                end;
            }
            action(Boton)
            {
                ApplicationArea = All;
                Caption = 'Ejercicio Codigo en Filtro';

                trigger OnAction()
                var
                    xlPagFiltros: FilterPageBuilder;
                    rlCustomer: Record Customer;
                    xMin: code[20];
                    xMax: code[20];
                    clClien: Label 'Clientes';
                    xCodigo: Code[20];
                begin
                    xlPagFiltros.PageCaption('Indicar parametros');
                    xlPagFiltros.AddRecord(clClien, rlCustomer);
                    xlPagFiltros.AddField(clClien, rlCustomer."No.");
                    xlPagFiltros.AddField(clClien, rlCustomer."Ship-to Filter");
                    if xlPagFiltros.RunModal() then begin
                        rlCustomer.SetView(xlPagFiltros.GetView(clClien, true));
                        xCodigo := rlCustomer.GetFilter("No.");
                        // xMax := 'zzzzzzzz';
                        // if rlCustomer.GetRangeMin("Ship-to Filter") <> '' then
                        //     xMin := rlCustomer.GetRangeMin("Ship-to Filter");
                        // if rlCustomer.GetRangeMax("Ship-to Filter") <> '' then
                        //     xMax := rlCustomer.GetRangeMax("Ship-to Filter");
                        // //
                        // if (xCodigo >= xMin) and (xCodigo <= xMax) then begin
                        //     // esta dentro
                        //     Message('El valor %1 esta DENTRO del Filtro %2',
                        //     xCodigo, rlCustomer."Ship-to Filter");
                        // end else begin
                        //     Message('El valor %1 esta FUERA del Filtro %2',
                        //     xCodigo, rlCustomer."Ship-to Filter");
                        // end;
                        if CumpleFiltroF(xCodigo, rlCustomer."Ship-to Filter") then begin
                            Message('El valor %1 esta DENTRO del Filtro %2',
                                    xCodigo, rlCustomer."Ship-to Filter");
                        end else begin
                            Message('El valor %1 esta FUERA del Filtro %2',
                            xCodigo, rlCustomer."Ship-to Filter");
                        end;
                    end;
                end;
            }
        }
    }
    local procedure ExportarTodo()
    var
        rlCompany: Record Company;
        rlTablas: Record AllObjWithCaption;
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;  // es una tabla que no guarda datos
        xlNombreFichero: Text;
        xlOutStr: OutStream;
        xlInStr: Instream;
        xlRecRef: RecordRef;
        xlTextBuilder: TextBuilder;
        cuTH: Codeunit "Type Helper";
        Ventana: Dialog;
        i: Integer;
    begin
        // bucle empresas
        CurrPage.SetSelectionFilter(rlCompany);
        if rlCompany.FindSet(false) then begin
            Ventana.Open('Empresa  #1###########\' +
                         'Tabla    #2###########\' +
                         'Registro #3###########');
            repeat
                // Inicio Empresa
                xlTextBuilder.Append('IE<' + rlCompany.Name + '>' + cuTH.CRLFSeparator());
                Ventana.Update(1, rlCompany.Name);
                // Bucle Tablas
                rlTablas.SetRange("Object Type", rlTablas."Object Type"::Table);
                rlTablas.SetFilter("Object ID", '%1|%2|%3|%4',
                                                Database::"G/L Account",
                                                Database::Customer,
                                                Database::Vendor,
                                                Database::Item);
                if rlTablas.FindSet(false) then begin
                    repeat
                        // Inicio Tabla
                        xlTextBuilder.Append('IT<' + rlTablas."Object Name" + '>' + cuTH.CRLFSeparator());
                        Ventana.Update(2, rlTablas."Object Name");
                        Ventana.Update(3, '');
                        // Bucle Registros
                        xlRecRef.Open(rlTablas."Object ID", false, rlCompany.Name);
                        if xlRecRef.FindSet(false) then begin
                            repeat
                                xlTextBuilder.Append('IR<' + Format(xlRecRef.RecordId) + '>' + cuTH.CRLFSeparator());
                                Ventana.Update(3, Format(xlRecRef.RecordId));
                                for i := 1 to xlRecRef.FieldCount do begin
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append('<' + xlRecRef.FieldIndex(i).Name + '>' + cSeparador);
                                            xlTextBuilder.Append('<' + format(xlRecRef.FieldIndex(i).Value) + '>' + cuTH.CRLFSeparator());
                                        end;
                                    end;
                                end;
                                xlTextBuilder.Append('FR<' + format(xlRecRef.RecordId) + '>' + cuTH.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;
                        xlRecRef.Close();
                        // Fin Tabla
                        xlTextBuilder.Append('FT<' + rlTablas."Object Name" + '>' + cuTH.CRLFSeparator());
                    until rlTablas.Next() = 0;
                end;
                xlTextBuilder.Append('FE<' + rlCompany.Name + '>' + cuTH.CRLFSeparator());
            until rlCompany.Next() = 0;
            Ventana.Close();
        end;
        // Escribir en el Fichero
        TempLConfigTMP.EjemBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        xlOutStr.WriteText(xlTextBuilder.ToText());
        TempLConfigTMP.EjemBlob.CreateInStream(xlInStr);
        xlNombreFichero := 'CopiaDatosBC.bak';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            error('No se ha podido descargar el fichero');
        end;
    end;

    local procedure ImportarDatosEmpresa()
    var
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;  // es una tabla que no guarda datos
        xlInStr: InStream;
        xlLinea: Text;
        xlRecRef: RecordRef;
        cuTH: Codeunit "Type Helper";
        Ventana: Dialog;
        xNum: Integer;
    begin
        if Confirm('Es conveniente restaurar la copia sobre una empresa VACIA\' +
                   'Si pulsa Aceptar se hará sobre la empresa %1,\\' +
                   'Esta seguro de iniciar el Proceso?', false, CompanyName) = true then begin
            //
            TempLConfigTMP.EjemBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
            if UploadIntoStream('fichero .bak a Importar|*.bak', xlInStr) then begin
                Ventana.Open('Tabla    #1###########\' +
                             'Registro #2###########');
                while not xlInStr.EOS do begin
                    xlInStr.ReadText(xlLinea);
                    case true of
                        xlLinea.ToUpper().StartsWith('IT<'):    // por si pudiera venir en minusculas
                            begin
                                // Abrir el registro
                                xlRecRef.Open(DameNumeroF(copystr(xlLinea, 4).TrimEnd('>')));
                                Ventana.Update(1, copystr(xlLinea, 4).TrimEnd('>'));
                            end;
                        xlLinea.ToUpper().StartsWith('IR<'):
                            begin
                                // Creamos el Registro
                                xlRecRef.Init();
                                Ventana.Update(2, copystr(xlLinea, 4).TrimEnd('>'));
                            end;
                        xlLinea.ToUpper().StartsWith('<'):
                            begin
                                // cada campo asignamos el valor
                                xNum := DameNumeroF(xlLinea.Split(cSeparador).Get(1).TrimEnd('>').TrimStart('<'), xlRecRef.Number);
                                if xlRecRef.Field(xNum).Class = xlRecRef.Field(xNum).Class::Normal then begin
                                    // No importamos los campos calculados, ni los FlowFilter
                                    xlRecRef.Field(xNum).Value := DevuelveCampoYvalorF(xlRecRef.Field(xNum), xlLinea.Split(cSeparador).Get(2).TrimEnd('>').TrimStart('<'));
                                end;
                            end;
                        xlLinea.ToUpper().StartsWith('FR<'):
                            begin
                                // Crear el registro
                                xlRecRef.Insert(false);
                            end;
                        xlLinea.ToUpper().StartsWith('FT<'):
                            begin
                                // Cerrar el Registro
                                xlRecRef.Close();
                            end;
                    end;
                end;
                Ventana.Close();
            end else begin
                Error('No se pudo subir el fichero a importar, Error: %1', GetLastErrorText());
            end;
        end;
    end;

    local procedure DevuelveCampoYvalorF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;
        rlCustomer: Record Customer;
        rlItem: Record Item;
        rlField: Record Field;
        xlBigInteger: BigInteger;
        xlDecim: Decimal;
        xlDate: Date;
        i: Integer;
        xlBool: Boolean;
        xlTime: Time;
        xlDateTim: DateTime;
        xlDura: Duration;
        xlGuid: Guid;
    begin
        case pFieldRef.Type of
            pFieldRef.Type::BigInteger:
                if Evaluate(xlBigInteger, pValor) then begin
                    exit(xlBigInteger);
                end;
            pFieldRef.Type::Integer:
                if Evaluate(xlBigInteger, pValor) then begin
                    exit(xlBigInteger);
                end;
            pFieldRef.Type::Decimal:
                if Evaluate(xlDecim, pValor) then begin
                    exit(xlDecim);
                end;
            pFieldRef.Type::Date:
                if Evaluate(xlDate, pValor) then begin
                    exit(xlDate);
                end;
            pFieldRef.Type::Boolean:
                if Evaluate(xlBool, pValor) then begin
                    exit(xlBool);
                end;
            pFieldRef.Type::Time:
                if Evaluate(xlTime, pValor) then begin
                    exit(xlTime);
                end;
            pFieldRef.Type::DateTime:
                if Evaluate(xlDateTim, pValor) then begin
                    exit(xlDateTim);
                end;
            pFieldRef.Type::Duration:
                if Evaluate(xlDura, pValor) then begin
                    exit(xlDura);
                end;
            pFieldRef.Type::Guid:
                if Evaluate(xlGuid, pValor) then begin
                    exit(xlGuid);
                end;
            pFieldRef.Type::Blob:
                exit(TempLConfigTMP.EjemBlob);
            pFieldRef.Type::Media:
                exit(rlCustomer.Image);
            pFieldRef.Type::MediaSet:
                exit(rlItem.Picture);
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin
                            exit(i);
                        end;
                    end;
                end;
            else
                exit(pValor);
        end;
        //
    end;

    local procedure SacaValorCampoF(pFieldRef: FieldRef; pValor: Text): Text
    begin
        if pFieldRef.Type = pFieldRef.Type::Option then begin
            ;
        end else begin
            exit(pValor);
        end;
    end;

    local procedure DameNumeroF(plLinea: Text): Integer
    var
        rlTablas: Record AllObjWithCaption;
    begin
        rlTablas.SetRange("Object Type", rlTablas."Object Type"::Table);
        rlTablas.Setrange("Object Name", plLinea);
        rlTablas.SetLoadFields("Object ID");
        if rlTablas.Findfirst() then begin
            exit(rlTablas."Object ID");
        end;
    end;

    local procedure DameNumeroF(plLinea: Text; pNumTabla: Integer): Integer
    var
        rlField: Record Field;
    begin
        rlField.Setrange(FieldName, plLinea);
        rlField.Setrange(TableNo, pNumTabla);
        rlField.SetLoadFields("No.");
        if rlField.Findfirst() then begin
            exit(rlField."No.");
        end;
    end;

    local procedure DatosTablaF(pRecVariant: Variant; pDrill: Boolean): Integer
    var
        xlRecRef: RecordRef;
        rlCompany: Record Company;
        culTypeHelper: Codeunit "Type Helper";
    begin
        if pRecVariant.IsRecord then begin
            xlRecRef.GetTable(pRecVariant); // pasa la tabla que trae al RecordRef
            if rlCompany.get(Rec.Name) then begin // si crean empresa daria error
                if Rec.Name <> CompanyName then begin
                    xlRecRef.ChangeCompany(Rec.Name);
                end;
                if pDrill then begin
                    pRecVariant := xlRecRef;
                    page.run(0, pRecVariant);
                end else begin
                    exit(xlRecRef.count);
                end;
            end;
        end;
    end;

    // Forma antigua, antes de RecordRef
    local procedure NumRegsF(pTabla: Integer; pDrill: Boolean): Integer
    begin
        case pTabla of
            13:
                begin
                    if Rec.Name <> CompanyName then rSalesPerson.ChangeCompany(Rec.Name);
                    if pDrill then begin
                        page.run(0, rSalesPerson);
                    end else begin
                        exit(rSalesPerson.count);
                    end;
                end;
            15:
                begin
                    if Rec.Name <> CompanyName then rGLAcount.ChangeCompany(Rec.Name);
                    if pDrill then begin
                        page.run(0, rGLAcount);
                    end else begin
                        exit(rGLAcount.count);
                    end;
                end;
            18:
                begin
                    if Rec.Name <> CompanyName then rCustomer.ChangeCompany(Rec.Name);
                    if pDrill then begin
                        page.run(0, rCustomer);
                    end else begin
                        exit(rCustomer.count);
                    end;
                end;
            23:
                begin
                    if Rec.Name <> CompanyName then rVendor.ChangeCompany(Rec.Name);
                    if pDrill then begin
                        page.run(0, rVendor);
                    end else begin
                        exit(rVendor.count);
                    end;
                end;
            27:
                begin
                    if Rec.Name <> CompanyName then rItem.ChangeCompany(Rec.Name);
                    if pDrill then begin
                        page.run(0, rItem);
                    end else begin
                        exit(rItem.count);
                    end;
                end;
        end;
    end;

    local procedure EjemploRecordRefsF()
    var
        xlRecRef: RecordRef;
        // sistema antiguo, cuando no se podia dotear un objeto (lo que lleva tras el punto)
        xlFieldRef: FieldRef;
        i: Integer;
    begin
        xlRecRef.Open(Database::Item);
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    message('%1', xlRecRef.FieldIndex(i).Caption);
                    message('%1', format(xlRecRef.FieldIndex(i).Value));
                end;
                //  cambiar a mayusculas el campo 3, metodo nuevo doteando el objeto
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3)).ToUpper());
                // antes se hacia asi, porque no se podia dotear
                xlFieldRef := xlRecRef.Field(3);
                xlFieldRef.Validate(uppercase(Format(xlFieldRef)));
            until xlRecRef.Next() = 0;
        end;
    end;

    procedure CumpleFiltroF(pCodigo: Code[20]; pFiltro: Text): Boolean
    var
        TempLCustomer: Record customer temporary;  // importante temporary para que no guarde nada
    begin
        TempLCustomer.Init();
        TempLCustomer."No." := pCodigo;
        TempLCustomer.Insert(true);
        TempLCustomer.SetFilter("No.", pFiltro);
        exit(not TempLCustomer.IsEmpty);
    end;

    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;
        rGLAcount: Record "G/L Account";
        rItem: Record Item;
        rSalesPerson: Record "Salesperson/Purchaser";
        cSeparador: Label '·', Locked = true;
}