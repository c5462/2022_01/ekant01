page 51013 "ECPAllObjetsC01"
{
    ApplicationArea = All;
    Caption = 'Lista de Objetos';
    PageType = List;
    SourceTable = AllObjWithCaption;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Object Type"; Rec."Object Type")
                {
                    ToolTip = 'Specifies the object type.';
                    ApplicationArea = All;
                }
                field("Object ID"; Rec."Object ID")
                {
                    ToolTip = 'Specifies the object ID.';
                    ApplicationArea = All;
                }
                field("Object Name"; Rec."Object Name")
                {
                    ToolTip = 'Specifies the name of the object.';
                    ApplicationArea = All;
                }
                field("Object Caption"; Rec."Object Caption")
                {
                    ToolTip = 'Specifies the caption of the object.';
                    ApplicationArea = All;
                }
                field("Object Subtype"; Rec."Object Subtype")
                {
                    ToolTip = 'Specifies the subtype of the object.';
                    ApplicationArea = All;
                }
                field("App Package ID"; Rec."App Package ID")
                {
                    ToolTip = 'Specifies the value of the App Package ID field.';
                    ApplicationArea = All;
                }
                field("App Runtime Package ID"; Rec."App Runtime Package ID")
                {
                    ToolTip = 'Specifies the value of the App Runtime Package ID field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
