page 51008 "ECPEntradaDeDatosVariosC01"
{
    Caption = 'Entrada de Datos Varios';
    PageType = StandardDialog;

    layout
    {
        area(content)
        {
            field(Texto01; mTexArray[1])
            {
                ApplicationArea = All;
                Visible = xTxtVisible01;
                CaptionClass = mCaptionClass[xTipoDato::texto, 1];
            }
            field(Texto02; mTexArray[2])
            {
                ApplicationArea = all;
                Visible = xTxtVisible02;
                CaptionClass = mCaptionClass[xTipoDato::texto, 2];
            }
            field(Texto03; mTexArray[3])
            {
                ApplicationArea = all;
                Visible = xTxtVisible03;
                CaptionClass = mCaptionClass[xTipoDato::texto, 3];
            }
            field(Texto04; mTexArray[4])
            {
                ApplicationArea = all;
                Visible = xTxtVisible04;
                CaptionClass = mCaptionClass[xTipoDato::texto, 4];
            }
            field(Bool01; mBooArray[1])
            {
                ApplicationArea = all;
                Visible = BooVisible1;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 1];
            }
            field(Bool02; mBooArray[2])
            {
                ApplicationArea = all;
                Visible = BooVisible2;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 2];
            }
            field(Bool03; mBooArray[3])
            {
                ApplicationArea = all;
                Visible = BooVisible3;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 3];
            }
            field(Dec01; mDecArray[1])
            {
                ApplicationArea = all;
                Visible = DecVisible1;
                CaptionClass = mCaptionClass[xTipoDato::numerico, 1];
            }
            field(Dec02; mDecArray[2])
            {
                ApplicationArea = all;
                Visible = DecVisible2;
                CaptionClass = mCaptionClass[xTipoDato::numerico, 2];
            }
            field(Dec03; mDecArray[3])
            {
                ApplicationArea = all;
                Visible = DecVisible3;
                CaptionClass = mCaptionClass[xTipoDato::numerico, 3];
            }
            field(Fec01; mFecArray[1])
            {
                ApplicationArea = all;
                Visible = FecVisible1;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 1];
            }
            field(Fec02; mFecArray[2])
            {
                ApplicationArea = all;
                Visible = FecVisible2;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 2];
            }
            field(Fec03; mFecArray[3])
            {
                ApplicationArea = all;
                Visible = FecVisible3;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 3];
            }
            field(Pass01; mPasArray[1])
            {
                ApplicationArea = all;
                Visible = PasVisible1;
                ExtendedDataType = Masked;
                CaptionClass = mCaptionClass[xTipoDato::Password, 1];
            }
            field(Pass02; mPasArray[2])
            {
                ApplicationArea = all;
                Visible = PasVisible2;
                ExtendedDataType = Masked;
                CaptionClass = mCaptionClass[xTipoDato::Password, 2];
            }
            field(Pass03; mPasArray[3])
            {
                ApplicationArea = all;
                Visible = PasVisible3;
                ExtendedDataType = Masked;
                CaptionClass = mCaptionClass[xTipoDato::Password, 3];
            }

        }
    }
    var
        xPass: Text;
        xTipoDato: option null,texto,Booleano,Numerico,Hora,Fecha,Password;
        xTipoPuntero: option null,Pasado,Leido;
        mCaptionClass: array[8, 10] of Text;
        mTexArray: array[3] of Text;
        mBooArray: array[3] of Boolean;
        mDecArray: Array[5] of Decimal;
        mFecArray: Array[5] of Date;
        mPasArray: Array[5] of Text;
        xTxtVisible01: Boolean;
        xTxtVisible02: Boolean;
        xTxtVisible03: Boolean;
        xTxtVisible04: Boolean;
        BooVisible1: Boolean;
        BooVisible2: Boolean;
        BooVisible3: Boolean;
        decVisible1: Boolean;
        decVisible2: Boolean;
        decVisible3: Boolean;
        FecVisible1: Boolean;
        FecVisible2: Boolean;
        FecVisible3: Boolean;
        PasVisible1: Boolean;
        PasVisible2: Boolean;
        PasVisible3: Boolean;
        mPunteros: array[8, 10] of Integer;

    /// <summary>
    /// Muestra una ventana de entrada de datos con los campos pasados como parametros
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text)
    var
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::texto] += 1;
        mCaptionClass[xTipoDato::texto, mPunteros[xTipoPuntero::pasado, xTipoDato::texto]] := pCaption;
        mTexArray[mPunteros[xTipoPuntero::pasado, xTipoDato::texto]] := pValorInicial;
        xTxtVisible01 := mPunteros[xTipoPuntero::pasado, xTipoDato::texto] >= 1;
        xTxtVisible02 := mPunteros[xTipoPuntero::pasado, xTipoDato::texto] >= 2;
        xTxtVisible03 := mPunteros[xTipoPuntero::pasado, xTipoDato::texto] >= 3;
        xTxtVisible04 := mPunteros[xTipoPuntero::pasado, xTipoDato::texto] >= 4;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: Boolean)
    var
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano] += 1;
        mCaptionClass[xTipoDato::Booleano, mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano]] := pCaption;
        mBooArray[mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano]] := pValorInicial;
        BooVisible1 := mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano] >= 1;
        BooVisible2 := mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano] >= 2;
        BooVisible3 := mPunteros[xTipoPuntero::pasado, xTipoDato::Booleano] >= 3;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: Decimal)
    var
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::numerico] += 1;
        mCaptionClass[xTipoDato::numerico, mPunteros[xTipoPuntero::pasado, xTipoDato::numerico]] := pCaption;
        mDecArray[mPunteros[xTipoPuntero::pasado, xTipoDato::numerico]] := pValorInicial;
        decVisible1 := mPunteros[xTipoPuntero::pasado, xTipoDato::numerico] >= 1;
        decVisible2 := mPunteros[xTipoPuntero::pasado, xTipoDato::numerico] >= 2;
        decVisible3 := mPunteros[xTipoPuntero::pasado, xTipoDato::numerico] >= 3;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: date)
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::Fecha] += 1;
        mCaptionClass[xTipoDato::Fecha, mPunteros[xTipoPuntero::pasado, xTipoDato::Fecha]] := pCaption;
        mFecArray[mPunteros[xTipoPuntero::pasado, xTipoDato::Fecha]] := pValorInicial;
        FecVisible1 := mPunteros[xTipoPuntero::pasado, xTipoDato::Fecha] >= 1;
        FecVisible2 := mPunteros[xTipoPuntero::pasado, xTipoDato::Fecha] >= 2;
        FecVisible3 := mPunteros[xTipoPuntero::pasado, xTipoDato::Fecha] >= 3;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: text; pPass: Boolean)
    begin
        mPunteros[xTipoPuntero::pasado, xTipoDato::Password] += 1;
        mCaptionClass[xTipoDato::Password, mPunteros[xTipoPuntero::pasado, xTipoDato::Password]] := pCaption;
        mPasArray[mPunteros[xTipoPuntero::pasado, xTipoDato::Password]] := pValorInicial;
        PasVisible1 := mPunteros[xTipoPuntero::pasado, xTipoDato::Password] >= 1;
        PasVisible2 := mPunteros[xTipoPuntero::pasado, xTipoDato::Password] >= 2;
        PasVisible3 := mPunteros[xTipoPuntero::pasado, xTipoDato::Password] >= 3;
    end;

    // salidas
    procedure CampoF(var pSalida: Date): date;
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha] += 1;
        pSalida := mFecArray[mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha]];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Date; pOrden: Integer): date;
    begin
        pSalida := mFecArray[pOrden];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Decimal): Decimal;
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::numerico] += 1;
        pSalida := mDecArray[mPunteros[xTipoPuntero::Leido, xTipoDato::numerico]];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Decimal; pOrden: Integer): Decimal;
    begin
        pSalida := mDecArray[pOrden];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Boolean): Boolean;
    var

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano] += 1;
        pSalida := mBooArray[mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano]];
        exit(pSalida)
    end;

    procedure CampoF(var pSalida: Boolean; pOrden: Integer): Boolean;
    begin
        pSalida := mBooArray[pOrden];
        exit(pSalida)
    end;

    procedure CampoF(): Text;
    var

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::texto] += 1;
        exit(mTexArray[mPunteros[xTipoPuntero::Leido, xTipoDato::texto]]);
    end;

    procedure CampoF(pOrden: Integer): Text;
    begin
        exit(mTexArray[pOrden]);
    end;

    procedure CampoF(var pSalida: Text; pOrden: Integer): Text;
    var

    begin
        exit(mPasArray[pOrden]);
    end;

}
