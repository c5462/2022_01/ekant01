page 51000 "ECPConfiguracionC01"
{
    Caption = 'Configuracion de la app 01 del curso';
    PageType = Card;
    SourceTable = ECPConfiguracionC01;
    AboutText = 'Aqui se definen los aspectos fijos de configuracion de la app';
    AboutTitle = 'Apartado de configuracion';
    AdditionalSearchTerms = 'Config. de mi primera app';
    UsageCategory = Administration;
    ApplicationArea = All;
    DeleteAllowed = false;
    DelayedInsert = false;
    ModifyAllowed = true;
    PopulateAllFields = true;
    ShowFilter = true;
    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Tooltip para explicar que aqui se define el codigo de cliente que se utilizara para los pedidos que entren por la web';
                    ApplicationArea = All;
                    AboutTitle = 'Campo para el codigo de cliente web';
                    AboutText = 'Aqui se define el codigo de cliente que se utilizara para los pedidos que entren por la web';
                    Importance = Promoted;
                }

                field(NomCliWeb; rec.NombreClienteF(Rec.CodCteWeb))
                {
                    Caption = 'Nombre del cliente por Funcion';
                    ToolTip = 'Guarda el Nombre del Cliente WEB.';
                    ApplicationArea = All;
                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Indicar el Texto para registrar.';
                    ApplicationArea = All;
                    AboutText = 'Indicar el texto para el apunte contable';
                    AboutTitle = 'Texto para Contabilizar apunte';
                }
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Indicar el Tipo de Asunto.';
                    ApplicationArea = All;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Tipo de Tabla.';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Codigo de Tabla.';
                    ApplicationArea = All;
                }
                field(Nombre; Rec.NombreEnTablaF(rec.TipoTabla, Rec.CodTabla))
                {
                    ToolTip = 'Codigo de Tabla.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Color Letra.';
                    ApplicationArea = All;
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Color Fondo.';
                    ApplicationArea = All;
                }
                field(Password; Rec.Password)
                {
                    ToolTip = 'Specifies the value of the Password general field.';
                    ApplicationArea = All;
                }

            }
            group(InfoInterna)
            {
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                    Editable = false;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                    Editable = false;
                }
            }
            group(CamposCalculados)
            {
                Caption = 'Campos calculados';
                field(NomCli2; Rec.NombreCliente)
                {
                    ToolTip = 'Es el nombre del Cliente.';
                    ApplicationArea = All;
                }
                field(NumUnidadesVendidas; Rec.UnidadesDisponibles)
                {
                    ToolTip = 'Specifies the value of the Nº unidades vendidas field.';
                    ApplicationArea = All;
                }
                field(NumIteraciones; xNumIteraciones)
                {
                    ApplicationArea = All;
                    Caption = 'Numero de Iteraciones';
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Diccionarios)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo Diccionarios';
                Image = AboutNav;
                trigger OnAction()
                begin
                    page.Run(page::ECPEjemploDiccionarioC01);
                end;
            }
            action(DescargarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Exportar Vacunas a Fichero';
                trigger OnAction()
                begin
                    ExportarVacunasF();
                end;
            }
            action(ImportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Importar Vacunas desde Fichero';
                trigger OnAction()
                begin
                    ImportarVacunasF();
                end;
            }
            action(PruebasConText)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con Text (TextBuilder)';

                trigger OnAction()
                var
                    i: Integer;
                    xlInicio: DateTime;
                    xlTxt: Text;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt += '.';
                    end;
                    Message('Tiempo invertido %1', CurrentDateTime - xlInicio);
                end;
            }
            action(PruebaConTextBuilder)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con TextBuilder';

                trigger OnAction()
                var
                    i: Integer;
                    xlInicio: DateTime;
                    xlTxt: TextBuilder;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumIteraciones do begin
                        xlTxt.Append('.');
                    end;
                    Message('Tiempo invertido %1', CurrentDateTime - xlInicio);
                end;
            }
            action(ContarCliConMovs)
            {
                ApplicationArea = All;
                Caption = 'Contar Clientes con Movimientos';

                trigger OnAction()
                var
                    xlPagFiltros: FilterPageBuilder;
                    rlMovs: Record "Cust. Ledger Entry";
                    xLista: List of [Code[20]];
                    xlDic: Dictionary of [Code[20], Decimal];
                    xlDic2: Dictionary of [Code[20], List of [Decimal]];
                    clMovs: Label 'Movimientos';
                begin
                    xlPagFiltros.PageCaption('Seleccione el cliente a procesar');
                    xlPagFiltros.AddRecord(clMovs, rlMovs);
                    xlPagFiltros.AddField(clMovs, rlMovs."Posting Date");
                    if xlPagFiltros.RunModal() then begin
                        rlMovs.SetView(xlPagFiltros.GetView(clMovs));  // trae los filtros
                        rlMovs.SetLoadFields("Sell-to Customer No.");  // esto carga solo un campo en la llamada a SQL y es mas rapido
                        if rlMovs.FindSet(false) then begin
                            repeat
                                if not xLista.Contains(rlMovs."Sell-to Customer No.") then begin
                                    xLista.Add(rlMovs."Sell-to Customer No.");
                                end;
                            until rlMovs.Next() = 0;
                            Message('Numero de veces encontrado %1', xLista.Count);
                        end else begin
                            Message('%1 No tiene movimientos', rlMovs."Sell-to Customer No.");
                        end;
                    end else begin
                        Error('Proceso cancelado');
                    end;
                end;
            }
        }
    }
    trigger OnOpenPage()
    begin
        rec.GetF();
    end;

    local procedure ExportarVacunasF()
    var
        rlCabVacunas: Record ECPPlanesVacunacionC01;
        rlLinVacunas: Record ECPLinPlanVacunacionC01;
        //rlTipo: Record ECPVariosC01;
        lTipo: Enum ECPtipoC01;
        plLista: Page ECPListaPlanesVacunacionC01;
        xlInStr: InStream;
        xlOutStr: OutStream;
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;  // es una tabla que no guarda datos
        xlLinea: Text;
        xlNombreFichero: Text;
        clSeparator: Label '·', Locked = true;
    begin
        Message('Seleccionar el Plan de Vacunacion a Exportar');
        // el cero en num.objeto indica que debe usar la lista asociada a la tabla como LookupPageId=xx
        // if Page.RunModal(0, rlCabVacunas) in [Action::LookupOK, Action::Yes, Action::OK] then begin
        plLista.LookupMode(true);
        plLista.SetRecord(rlCabVacunas);
        if plLista.RunModal() in [Action::LookupOK, Action::Yes, Action::OK] then begin
            // obtenemos el DataSet
            plLista.GetSelectionFilterF(rlCabVacunas);
            Message('Num registros devueltos es %1', rlCabVacunas.Count);
            TempLConfigTMP.EjemBlob.CreateOutStream(xlOutStr, TextEncoding::Windows);
            // lo recorremos
            if rlCabVacunas.Findset(false, false) then begin  // p1: modif, p2: cambiamos key princ
                repeat
                    xlOutStr.WriteText('C' + clSeparator +
                                        rlCabVacunas.CodigoCab + clSeparator +
                                        rlCabVacunas.Descripcion + clSeparator +
                                        rlCabVacunas.EmpresaVacunadora + clSeparator +
                                        rlCabVacunas.NombreEmpresaF(rlCabVacunas.EmpresaVacunadora) + clSeparator + format(rlCabVacunas.FechaInicioVacunacionPlanif));
                    xlOutStr.WriteText(); // este manda un 13,10 CR,LF
                    rlLinVacunas.SetRange(CodigoLin, rlCabVacunas.CodigoCab);
                    if rlLinVacunas.FindSet(false) then begin
                        repeat
                            xlOutStr.WriteText('L' + clSeparator +
                                        rlLinVacunas.CodigoLin + clSeparator +
                                        format(rlLinVacunas.Linea) + clSeparator +
                                        rlLinVacunas.ClienteAvacunar + clSeparator +
                                        rlLinVacunas.NombreClienteF() + clSeparator +
                                        rlLinVacunas.CodigoVacuna + clSeparator +
                                        rlLinVacunas.NombreOtrosF(lTipo::Vacuna, rlLinVacunas.CodigoVacuna) + clSeparator +
                                        rlLinVacunas.CodigoOtros + clSeparator +
                                        rlLinVacunas.NombreOtrosF(lTipo::Otros, rlLinVacunas.CodigoOtros) + clSeparator +
                                        format(rlLinVacunas.FechaVacunacion) + clSeparator +
                                        format(rlLinVacunas.FechaSigteDosis));
                            xlOutStr.WriteText(); // este manda un 13,10 CR,LF
                        until rlLinVacunas.next = 0;
                    end;
                until rlCabVacunas.next = 0;
                TempLConfigTMP.EjemBlob.CreateInStream(xlInStr);
                //xlInStr.ReadText(xlLinea);  // lee y carga a traves de la pajita
                xlNombreFichero := 'CabVacunas.txt';
                if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
                    error('No se ha podido descargar el fichero');
                end;
            end;
        end else begin
            Error('Cancelado por el usuario');
        end;
        // // escribe a traves de la pajita
    end;

    local procedure ImportarVacunasF()
    var
        rlCabVacunas: Record ECPPlanesVacunacionC01;
        rlLinVacunas: Record ECPLinPlanVacunacionC01;
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;  // es una tabla que no guarda datos
        xlInStr: InStream;
        clSeparator: Label '·', Locked = true;
        xlLinea: Text;
        Ventana: Dialog;
        xlFecha: Date;
        xlLin: Integer;
    begin
        TempLConfigTMP.EjemBlob.CreateInStream(xlInStr, TextEncoding::Windows);
        if UploadIntoStream('', xlInStr) then begin
            Ventana.Open('Importando #1################');
            while not xlInStr.EOS do begin
                xlInStr.ReadText(xlLinea);
                Ventana.Update(1, xlLinea);
                case true of
                    xlLinea[1] = 'C':
                        begin
                            rlCabVacunas.init;
                            rlCabVacunas.validate(CodigoCab, xlLinea.Split(clSeparator).Get(2));
                            rlCabVacunas.Insert(true);
                            rlCabVacunas.Validate(Descripcion, xlLinea.Split(clSeparator).Get(3));
                            rlCabVacunas.Validate(EmpresaVacunadora, xlLinea.Split(clSeparator).Get(4));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparator).Get(6)) then begin
                                rlCabVacunas.Validate(FechaInicioVacunacionPlanif, xlFecha);
                            end;
                            rlCabVacunas.modify(true);
                        end;
                    xlLinea[1] = 'L':
                        begin
                            rlLinVacunas.Init();
                            rlLinVacunas.validate(CodigoLin, xlLinea.Split(clSeparator).Get(2));
                            if Evaluate(xlLin, xlLinea.Split(clSeparator).Get(3)) then begin
                                rlLinVacunas.validate(Linea, xlLin);
                            end;
                            rlLinVacunas.Insert(true);
                            rlLinVacunas.Validate(ClienteAvacunar, xlLinea.Split(clSeparator).Get(4));
                            rlLinVacunas.Validate(CodigoVacuna, xlLinea.Split(clSeparator).Get(6));
                            rlLinVacunas.Validate(CodigoOtros, xlLinea.Split(clSeparator).Get(8));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparator).Get(10)) then begin
                                rlLinVacunas.Validate(FechaVacunacion, xlFecha);
                            end;
                            if Evaluate(xlFecha, xlLinea.Split(clSeparator).Get(11)) then begin
                                rlLinVacunas.Validate(FechaSigteDosis, xlFecha);
                            end;
                            rlLinVacunas.modify(true);
                        end;
                end;
            end;
            Ventana.Close();
            //
        end else begin
            //
        end;
    end;

    var
        xNumIteraciones: Integer;
}
