/// <summary>
/// Cabecera Plan Vacunacion
/// </summary>
page 51004 "ECPFichaCabPlanVacunacionC01"
{
    Caption = 'Ficha Plan Vacunacion';
    PageType = Document;
    SourceTable = ECPPlanesVacunacionC01;
    SaveValues = true;    // para que guarde la vble xPassword
    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.CodigoCab)
                {
                    Caption = 'Código';
                    ToolTip = 'Specifies the value of the Codigo field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    Caption = 'Descripción';
                    ToolTip = 'Specifies the value of the Descripcion field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    Caption = 'Empresa Vacunadora';
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(FechaInicioVacunacionPlanif; Rec.FechaInicioVacunacionPlanif)
                {
                    Caption = 'Fecha Inicio';
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    Caption = 'Bloqueado';
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVac; Rec.NombreEmpresaF(Rec.EmpresaVacunadora))
                {
                    Caption = 'Nombre Empresa Vacunadora';
                    ToolTip = 'Es el nombre del proveedor que hace la vacunacion.';
                    ApplicationArea = All;
                }
            }
            part(Lineas; ECPSubPageLineasVacC01)
            {
                ApplicationArea = All;
                SubPageLink = CodigoLin = field(CodigoCab);
            }
            group(Info)
            {

                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(EjemploVble)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de Variables Texto';
                Promoted = true;
                trigger OnAction()
                var
                    culFunc: Codeunit ECPFuncionesAppC01;
                    xlTexto: Text;
                begin
                    xlTexto := 'Texto de Entrada';
                    culFunc.EjemploVariablesEntSalidaF(xlTexto);
                    Message(xlTexto);
                end;
            }
            action(EjemploVbleBuilder)
            {
                ApplicationArea = All;
                Caption = 'Ejemplo de Variables TextBuilder';
                Promoted = true;
                trigger OnAction()
                var
                    culFunc: Codeunit ECPFuncionesAppC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('Texto de Entrada.');
                    culFunc.EjemploVariablesEntSalida2F(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }
            action(Bingo)
            {
                ApplicationArea = All;
                Caption = 'Bingo ';
                Promoted = true;
                trigger OnAction()
                var
                    culBingo: Codeunit ECPFuncionesAppC01;
                begin
                    culBingo.EjemploArrayF();
                end;
            }
            action(PruebaFncSegPlano)
            {
                ApplicationArea = All;
                Caption = 'Prueba Funcion en Segundo Plano ';
                Promoted = true;
                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    StartSession(xlSesionIniciada, Codeunit::ECPCodeunitLentaC01);
                    // esto lanza un proceso en segundo plano y sigue con el proceso
                    Message('Proceso lanzado en segundo plano');
                end;
            }
            action(PruebaPrimerPlano)
            {
                ApplicationArea = All;
                Caption = 'Prueba Funcion en Primer Plano ';
                Promoted = true;
                trigger OnAction()
                begin
                    Codeunit.Run(Codeunit::ECPCodeunitLentaC01);
                    Message('Proceso terminado');
                end;
            }
            action(Logs)
            {
                ApplicationArea = All;
                Caption = 'Logs';
                Promoted = true;
                Image = Log;
                RunObject = page ECPListaLogsC01;
            }
            action(PedirDatos)
            {
                ApplicationArea = All;
                Caption = 'Pedir Datos Varios';
                trigger OnAction()
                var
                    pglPagina: page ECPEntradaDeDatosVariosC01;
                    xlBool: Boolean;
                    xlDec: Decimal;
                begin
                    //Page.RunModal(page::ECPEntradaDeDatosVariosC01);
                    pglPagina.CampoF('Texto 1', 'Albacete');
                    pglPagina.CampoF('Texto 2', 'España');
                    pglPagina.CampoF('Texto 3', '');
                    pglPagina.CampoF('¿Cuota pagada?', false);
                    pglPagina.CampoF('¿coche pagado?', true);
                    pglPagina.CampoF('¿casa pagada?', true);
                    pglPagina.CampoF('Importe cuota', 10);
                    pglPagina.CampoF('Importe 2', 4);
                    pglPagina.CampoF('Importe 3', 200);
                    if pglPagina.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
                        Message('Texto 1: %1', pglPagina.CampoF());
                        Message('Texto 2: %1', pglPagina.CampoF());
                        Message('Texto 3: %1', pglPagina.CampoF());
                        Message('Cuota %1', pglPagina.CampoF(xlBool));
                        Message('Coche %1', pglPagina.CampoF(xlBool));
                        Message('Casa %1', pglPagina.CampoF(xlBool));
                        Message('Imp 1 %1', pglPagina.CampoF(xlDec));
                        Message('Imp 2 %1', pglPagina.CampoF(xlDec));
                        Message('Imp 3 %1', pglPagina.CampoF(xlDec));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }
            action(CambiarPassword)
            {
                ApplicationArea = All;
                Caption = 'Cambiar Password';
                trigger OnAction()
                begin
                    //Page.RunModal(page::ECPEntradaDeDatosVariosC01);
                    CambPasswordF();
                end;
            }
            action(CrearLineas)
            {
                ApplicationArea = All;
                Caption = 'Crear Lineas';
                Image = Create;
                trigger OnAction()
                var
                    rLin: Record ECPLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 10000 do begin
                        rLin.Init();
                        rLin.validate(CodigoLin, Rec.CodigoCab);
                        rLin.validate(Linea, i * 10000);
                        rLin.Insert(true);
                    end;
                    Message('Inicio: %1\Fin %2\Tiempo %3', xlInicio, CurrentDateTime, CurrentDateTime - xlInicio);
                end;
            }
            action(CrearLineasNoBulk)
            {
                ApplicationArea = All;
                Caption = 'Crear Lineas NoBulk';
                Image = Create;
                trigger OnAction()
                var
                    rLin: Record ECPLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 10000 do begin
                        rLin.Init();
                        rLin.validate(CodigoLin, Rec.CodigoCab);
                        rLin.validate(Linea, i * 10000);
                        if not rLin.Insert(true) then begin
                            Error('No se ha podido insertar la linea %1, del plan %2', rLin.CodigoLin, rLin.Linea);
                        end;
                    end;
                    Message('Inicio: %1\Fin %2\Tiempo %3', xlInicio, CurrentDateTime, CurrentDateTime - xlInicio);
                end;
            }
            action(ModificarLineas)
            {
                ApplicationArea = All;
                Caption = 'Modificar Lineas';
                Image = "8ball";
                trigger OnAction()
                var
                    rLin: Record ECPLinPlanVacunacionC01;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rLin.SetRange(CodigoLin, Rec.CodigoCab);
                    rLin.modifyall(FechaVacunacion, Rec.FechaInicioVacunacionPlanif, true);
                    Message('Tiempo %1', CurrentDateTime - xlInicio);
                end;
            }
            action(ModificarLineasNoBulk)
            {
                ApplicationArea = All;
                Caption = 'Modificar Lineas NoBulk';
                Image = Create;
                trigger OnAction()
                var
                    rLin: Record ECPLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    rLin.SetRange(CodigoLin, Rec.CodigoCab);
                    if rLin.FindSet(true, false) then begin
                        repeat
                            rLin.Validate(FechaVacunacion, Rec.FechaInicioVacunacionPlanif + 1);
                            rLin.modify(true);
                        until rLin.Next() = 0;
                    end;
                    Message('Tiempo %1', CurrentDateTime - xlInicio);
                end;
            }
        }
    }

    [NonDebuggable]  // afecta a la sigte funcion unicamente
    local procedure CambPasswordF()
    var
        pglPagina: page ECPEntradaDeDatosVariosC01;
        xlPass: Text;
        rlConfig: Record ECPConfiguracionC01;
    begin
        rlConfig.get();
        pglPagina.CampoF('Password Actual', '');
        pglPagina.CampoF('Password Nuevo', '');
        pglPagina.CampoF('Repetir Nuevo Pass', '');
        if pglPagina.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            if pglPagina.CampoF(1) = rlConfig.Password then begin
                Message('Pass Anterior: %1', pglPagina.CampoF());
                Message('Pass Nueva: %1', pglPagina.CampoF());
                rlConfig.PasswordF(xlPass);
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;
}
