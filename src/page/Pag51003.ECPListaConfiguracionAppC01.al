page 51003 "ECPListaConfiguracionAppC01"
{
    ApplicationArea = All;
    Caption = 'Lista Configuracion App';
    PageType = List;
    SourceTable = ECPConfiguracionC01;
    UsageCategory = Lists;
    ShowFilter = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Indicar el Texto para registrar.';
                    ApplicationArea = All;
                }
                field(UnidadesDisponibles; Rec.UnidadesDisponibles)
                {
                    ToolTip = 'Specifies the value of the Nº unidades vendidas field.';
                    ApplicationArea = All;
                }

            }
        }
    }
}
