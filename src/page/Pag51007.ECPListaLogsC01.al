page 51007 "ECPListaLogsC01"
{
    ApplicationArea = All;
    Caption = 'Lista de Logs';
    PageType = List;
    SourceTable = "ECPLogC01";
    SourceTableView = sorting(SystemCreatedAt) order(descending);
    UsageCategory = Lists;
    ModifyAllowed = false;
    InsertAllowed = false;
    DeleteAllowed = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {

                field(Mensaje; Rec.Mensaje)
                {
                    ToolTip = 'Indica el motivo del evento.';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Indica cuando se creó el evento.';
                    ApplicationArea = All;
                    AutoFormatType = 0;
                    AutoFormatExpression = '<Year4>-<Month,2>-<Day,2> <Hour>:<Minutes>:<Seconds>';
                }
            }
        }
    }

    trigger OnOpenPage()
    begin

    end;
}
