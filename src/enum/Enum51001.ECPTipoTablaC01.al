enum 51001 "ECPTipoTablaC01"
{
    value(0; Cliente)
    {
        Caption = 'Tabla Cliente';
    }
    value(10; Proveedor)
    {
        Caption = 'Tabla Proveedor';
    }
    value(20; Recurso)
    {
        Caption = 'Tabla Recurso';
    }
    value(30; Empleado)
    {
        Caption = 'Tabla Empleado';
    }
    value(40; Contacto)
    {
        Caption = 'Tabla Contacto';
    }
}
