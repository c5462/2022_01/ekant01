enum 51002 "ECPColoresC01"
{
    value(0; Blanco)
    {
        Caption = 'Blanco';
    }
    value(1; Negro)
    {
        Caption = 'Negro';
    }
    value(2; Gris)
    {
        Caption = 'Gris';
    }
    value(3; Azul)
    {
        Caption = 'Azul';
    }
}
