/// <summary>
/// Cabecera Planes de Vacunacion
/// </summary>
table 51002 "ECPPlanesVacunacionC01"
{
    Caption = 'Planes de Vacunacion';
    DataClassification = ToBeClassified;
    DataCaptionFields = CodigoCab, Descripcion;
    LookupPageId = ECPListaPlanesVacunacionC01;
    DrillDownPageId = ECPListaPlanesVacunacionC01;

    fields
    {
        field(1; CodigoCab; Code[20])
        {
            Caption = 'Codigo';
            DataClassification = CustomerContent;
        }
        field(20; Descripcion; Text[50])
        {
            Caption = 'Descripcion';
            DataClassification = CustomerContent;
        }
        field(30; FechaInicioVacunacionPlanif; Date)
        {
            Caption = 'Fecha Inicio Vacunacion Planificada';
            DataClassification = CustomerContent;
            trigger OnValidate()
            var
                rlLineas: Record ECPLinPlanVacunacionC01;
            begin
                if Rec.FechaInicioVacunacionPlanif <> xRec.FechaInicioVacunacionPlanif then begin
                    rlLineas.SetRange(CodigoLin, Rec.CodigoCab);
                    if rlLineas.findset then
                        repeat
                            if rlLineas.FechaVacunacion = 0D then begin
                                rlLineas.FechaVacunacion := Rec.FechaInicioVacunacionPlanif;
                                rlLineas.Modify(false);
                            end;
                        until rlLineas.Next() = 0;
                end;

            end;
        }
        field(40; EmpresaVacunadora; Code[20])
        {
            Caption = 'Código Empresa Vacunadora';
            DataClassification = CustomerContent;
            TableRelation = Vendor."No.";
        }
        field(50; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = CustomerContent;
        }
        field(60; NombreEmpresa; Text[100])
        {
            Caption = 'Nombre de la Empresa Vacunadora';
            Editable = false;
            FieldClass = FlowField;
            CalcFormula = lookup(Vendor.Name where("No." = field(EmpresaVacunadora)));
        }
    }
    keys
    {
        key(PK; CodigoCab)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; CodigoCab, Descripcion)
        {

        }
    }

    trigger OnDelete()
    var
        rlLin: Record ECPLinPlanVacunacionC01;
    begin
        Rec.TestField(Bloqueado, false);
        //
        if Confirm('Desea Eliminar las lineas asociadas?', false) = true then begin
            rlLin.SetRange(CodigoLin, Rec.CodigoCab);
            if not rlLin.IsEmpty then begin
                rlLin.DeleteAll(true);
            end;
            // esta forma es la mas optima
        end;
    end;
    /// <summary>
    /// NombreEmpresaF.
    /// </summary>
    /// <param name="pCodigo">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure NombreEmpresaF(pCodigo: Code[20]): Text
    var
        rlVendor: Record Vendor;
    begin
        if rlVendor.get(pCodigo) then exit(rlVendor.Name) else exit('');
    end;

}
