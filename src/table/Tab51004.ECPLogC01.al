table 51004 "ECPLogC01"
{
    Caption = 'Tabla de Logs';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; Id; guid)
        {
            Caption = 'Id del Log';
            DataClassification = SystemMetadata;
        }
        field(10; Mensaje; Text[250])
        {
            Caption = 'Mensaje a mostrar';
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
        key(PK2; SystemCreatedAt)
        {

        }

    }
    trigger OnInsert()
    begin
        if IsNullGuid(Rec.Id) then begin
            Rec.validate(Id, CreateGuid());
        end;
    end;

    procedure GuardaLogF(pText: Text)
    var
        rlLogError: Record ECPLogC01;
    begin
        rlLogError.Init();
        rlLogError.validate(Mensaje, copystr(pText, 1, MaxStrLen(rlLogError.Mensaje)));
        rlLogError.Insert(true);  // el true ya crea un Guid
    end;
}
