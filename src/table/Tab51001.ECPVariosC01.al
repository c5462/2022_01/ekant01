table 51001 "ECPVariosC01"
{
    Caption = 'Tabla Auxiliar para Codigo y Nombre';
    DataClassification = ToBeClassified;
    DataCaptionFields = Codigo, Descripcion;
    LookupPageId = ECPListaVariosC01;
    DrillDownPageId = ECPListaVariosC01;

    fields
    {
        field(1; Tipo; Enum ECPtipoC01)
        {
            Caption = 'Tipo';
            DataClassification = CustomerContent;
        }
        field(10; Codigo; Code[20])
        {
            Caption = 'Código';
            DataClassification = CustomerContent;
        }
        field(20; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = CustomerContent;
        }

        field(30; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = CustomerContent;
        }
        field(40; PeriodoVacunacion; DateFormula)
        {
            Caption = 'Periodo para siguiente dosis';
            DataClassification = CustomerContent;
        }
    }
    keys
    {
        key(PK; Tipo, Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion, PeriodoVacunacion)
        {

        }
        fieldgroup(Brik; Codigo, Descripcion, PeriodoVacunacion) // ladrillo
        {

        }
    }
    trigger OnDelete()
    begin
        // if rec.Bloqueado then begin
        //     Error('No se puede eliminar un registro no bloqueado');
        // end;
        Rec.TestField(Bloqueado, true);
    end;
}
