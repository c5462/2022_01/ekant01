table 51000 "ECPConfiguracionC01"
{
    Caption = 'Configuración de la App';
    DataClassification = OrganizationIdentifiableInformation;

    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id';
            DataClassification = SystemMetadata;
        }
        field(10; CodCteWeb; Code[20])
        {
            Caption = 'CodCteWeb';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            ValidateTableRelation = true;
        }
        field(20; TextoRegistro; Text[250])
        {
            Caption = 'TextoRegistro';
            DataClassification = SystemMetadata;
        }
        field(30; Tipo; Option)
        {
            Caption = 'Tipo';
            OptionMembers = "",Producto,Activo;
            DataClassification = SystemMetadata;
        }
        field(40; NombreCliente; Text[100])
        {
            Caption = 'Nombre del Cliente';
            FieldClass = FlowField;
            CalcFormula = lookup(Customer.Name where("No." = field(CodCteWeb)));
            Editable = false;
        }
        field(50; TipoTabla; Enum ECPTipoTablaC01)
        {
            Caption = 'Tipo de Tabla';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                if Rec.TipoTabla <> xRec.TipoTabla then begin
                    Rec.validate(CodTabla, '');
                end;
            end;
        }
        field(60; CodTabla; Code[20])
        {
            Caption = 'Código de Tabla';
            DataClassification = SystemMetadata;
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            if (TipoTabla = const(Proveedor)) Vendor else
            if (TipoTabla = const(Contacto)) Contact else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Recurso)) Resource;
        }

        field(70; ColorFondo; Enum ECPColoresC01)
        {
            Caption = 'Color del Fondo';
            DataClassification = SystemMetadata;
        }
        field(80; ColorLetra; Enum ECPColoresC01)
        {
            Caption = 'Color de las Letra';
            DataClassification = SystemMetadata;
        }
        field(100; Cod2; code[20])
        {
            Caption = 'Codigo 2';
            DataClassification = SystemMetadata;
        }
        field(110; UnidadesDisponibles; decimal)
        {
            Caption = 'Nº unidades disponibles';
            FieldClass = FlowField;
            CalcFormula = sum("Item Ledger Entry".Quantity where("Item No." = field(FiltroProducto), "Posting Date" = field(FiltroFecha), "Entry Type" = field(FiltroTipoMov)));
            DecimalPlaces = 0 : 5;
        }
        field(120; FiltroProducto; Code[20])
        {
            TableRelation = Item."No.";
            Caption = 'Filtro producto';
            FieldClass = FlowFilter;
        }
        field(130; FiltroFecha; date)
        {
            Caption = 'Filtro Fecha';
            FieldClass = FlowFilter;
        }
        field(140; FiltroTipoMov; enum "Item Ledger Entry Type")
        {
            Caption = 'Filtro Tipo Movimientos';
            FieldClass = FlowFilter;
        }

        field(150; Password; Guid)
        {
            DataClassification = SystemMetadata;
            //Caption = 'Password general';   // no se va a mostrar
            //FieldClass = FlowFilter;
            //TableRelation = "Item Ledger Entry"."Entry Type";
        }
        field(160; EjemBlob; Blob)
        {
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {

    }
    /// <summary>
    /// Recupera el Registro de la tabla actual y si no existe lo crea
    /// </summary>
    /// <returns></returns>
    procedure GetF(): Boolean
    begin
        if not Rec.Get() then begin
            Rec.Init();
            Rec.Insert(true);
        end;
        exit(true);
    end;

    /// <summary>
    /// Devuelve el nombre del Cliente de un codigo pasado como parametro
    /// </summary>
    /// <param name="pCodClte"></param>
    /// <returns></returns>
    procedure NombreClienteF(pCodClte: Code[20]): Text
    var
        rlCliente: Record Customer;
    begin
        if rlCliente.get(pCodClte) then exit(rlCliente.Name) else exit('');
    end;

    /// <summary>
    /// Saca el nombre del campo en funcion de Tipo de Tabla
    /// </summary>
    /// <param name="pTipoTabla">Enum ECPTipoTablaC01.</param>
    /// <param name="pCodTabla">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure NombreEnTablaF(pTipoTabla: Enum ECPTipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlConfig: Record ECPConfiguracionC01;
        rlCliente: Record Customer;
        rlProveedor: Record Vendor;
        rlEmpleado: Record Employee;
        rlRecurso: Record Resource;
        rlContacto: Record Contact;
    begin
        case pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(rlConfig.NombreClienteF(pCodTabla));
                end;
            pTipoTabla::Proveedor:
                begin
                    if rlProveedor.get(pCodTabla) then exit(rlProveedor.Name) else exit('');
                end;
            pTipoTabla::Empleado:
                begin
                    if rlEmpleado.get(pCodTabla) then exit(rlEmpleado.FullName()) else exit('');
                end;
            pTipoTabla::Recurso:
                begin
                    if rlRecurso.get(pCodTabla) then exit(rlRecurso.Name) else exit('');
                end;
            pTipoTabla::Contacto:
                begin
                    if rlContacto.get(pCodTabla) then exit(rlContacto.Name) else exit('');
                end;
        end;
    end;

    trigger OnInsert()
    begin

    end;

    [NonDebuggable]
    procedure PasswordF(pPassword: Text)
    begin
        Rec.Validate(Password, CreateGuid());
        Rec.Modify(true);
        IsolatedStorage.Set(Rec.Password, pPassword, DataScope::Company);
        // almacena en una zona especial de SQL a la que no se puede acceder con la clave GUID guardada, 
        // en la base de datos se guarda el Guid, no el password
    end;

    [NonDebuggable]
    procedure PasswordF() xSalida: Text
    begin
        if not IsolatedStorage.Get(Rec.Password, DataScope::Company, xSalida) then begin
            Error('No se encuentra el Password');
        end;
    end;
}
