/// <summary>
/// Table ECPLinPlanVacunacionC01 (ID 51003).
/// </summary>
table 51003 "ECPLinPlanVacunacionC01"
{
    Caption = 'Lineas Plan de Vacunacion';
    DataClassification = ToBeClassified;
    LookupPageId = ECPSubPageLineasVacC01;
    DrillDownPageId = ECPSubPageLineasVacC01;
    fields
    {
        field(1; CodigoLin; Code[20])
        {
            Caption = 'Código';
            DataClassification = CustomerContent;
        }
        field(20; Linea; Integer)
        {
            Caption = 'Nº Linea';
            DataClassification = CustomerContent;
        }
        field(30; ClienteAvacunar; Code[20])
        {
            Caption = 'Cliente a Vacunar';
            DataClassification = CustomerContent;
            TableRelation = Customer."No.";

            trigger OnValidate()
            begin
                if Rec.FechaVacunacion = 0D then begin
                    Rec.FechaVacunacion := CargarFecha(Rec.CodigoLin);
                end;
            end;
        }
        field(40; FechaVacunacion; Date)
        {
            Caption = 'Fecha Vacunacion';
            DataClassification = CustomerContent;
            trigger OnValidate()
            begin
                if Rec.FechaVacunacion <> xRec.FechaVacunacion then begin
                    CalcularSigteF();
                end;
            end;
        }
        field(50; CodigoVacuna; Code[20])
        {
            Caption = 'Tipo Vacuna';
            DataClassification = CustomerContent;
            TableRelation = ECPVariosC01.Codigo where(Tipo = const(Vacuna), Bloqueado = const(false));
            trigger OnValidate()
            begin
                if Rec.CodigoVacuna <> xRec.CodigoVacuna then begin
                    CalcularSigteF();
                end;
            end;
        }
        field(60; CodigoOtros; code[20])
        {
            Caption = 'Tipo Otros';
            DataClassification = CustomerContent;
            TableRelation = ECPVariosC01.Codigo where(Tipo = const(Otros), Bloqueado = const(false));
        }
        field(70; FechaSigteDosis; date)
        {
            Caption = 'Fecha prevista Sigte Dosis';
            DataClassification = CustomerContent;

        }

    }
    keys
    {
        key(PK; CodigoLin, Linea)
        {
            Clustered = true;
        }
    }

    procedure NombreClienteF(): Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.get(Rec.ClienteAvacunar) then begin
            exit(rlCustomer.Name);
        end;
    end;

    /// <summary>
    /// NombreOtros.
    /// </summary>
    /// <param name="pTipo">Enum ECPtipoC01.</param>
    /// <param name="pCodVarios">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure NombreOtrosF(pTipo: Enum ECPtipoC01; pCodVarios: Code[20]): Text
    var
        rlVarios: Record ECPVariosC01;
    begin
        if rlVarios.get(pTipo, pCodVarios) then begin
            exit(rlVarios.Descripcion);
        end;
    end;

    /// <summary>
    /// CargarFecha.
    /// </summary>
    /// <param name="pCod">Code[20].</param>
    /// <returns>Return value of type Date.</returns>
    procedure CargarFecha(pCod: Code[20]): Date
    var
        rlCab: Record ECPPlanesVacunacionC01;
    begin
        if rlCab.get(pCod) then begin
            exit(rlCab.FechaInicioVacunacionPlanif);
        end;
    end;

    local procedure CalcularSigteF()
    var
        rlVarios: Record ECPVariosC01;
        IsHandled: Boolean;
    begin
        /// <summary>
        /// OnBeforeCalcularFechaProxVacunaF.
        /// </summary>
        /// <param name="Rec">Record ECPLinPlanVacunacionC01.</param>
        /// <param name="xRec">Record ECPLinPlanVacunacionC01.</param>
        /// <param name="rlVarios">Record ECPVariosC01.</param>
        /// <param name="IsHandled">VAR Boolean.</param>
        OnBeforeCalcularFechaProxVacunaF(Rec, xRec, rlVarios, IsHandled);
        if not IsHandled then begin
            // es una forma de saber si quien se ha suscrito a esta funcion quiere que sigamos
            // con el proceso previsto o el ya ha hecho todo
            if Rec.FechaVacunacion <> 0D then begin
                if rlVarios.get(rlVarios.Tipo::Vacuna, Rec.CodigoVacuna) then begin
                    Rec.validate(FechaSigteDosis, CalcDate(rlVarios.PeriodoVacunacion, Rec.FechaVacunacion));
                end;
            end;
        end;
        OnAfterCalcularFechaProxVacunaF(Rec, xRec, rlVarios, IsHandled);
        // aqui no tiene mucho sentido hacer un isHandled, si no hay postproceso despues
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeCalcularFechaProxVacunaF(Rec: Record ECPLinPlanVacunacionC01; xRec: Record ECPLinPlanVacunacionC01; rlVarios: Record ECPVariosC01; var IsHandled: Boolean)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterCalcularFechaProxVacunaF(Rec: Record ECPLinPlanVacunacionC01; xRec: Record ECPLinPlanVacunacionC01; rlVarios: Record ECPVariosC01; var IsHandled: Boolean)
    begin
    end;



}
