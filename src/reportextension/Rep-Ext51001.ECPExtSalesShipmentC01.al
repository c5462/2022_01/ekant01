reportextension 51001 "ECPExtSalesShipmentC01" extends "Sales - Shipment"
{

    dataset
    {
        addlast("Sales Shipment Header")
        {
            dataitem(SalesShipmentHeader; "Sales Shipment Header")
            {

                column(ECPShiptoName_SalesShipmentHeader; "Ship-to Name")
                {
                }

                column(ECPShiptoAddress_SalesShipmentHeader; "Ship-to Address")
                {
                }
                column(ECPShiptoCity_SalesShipmentHeader; "Ship-to City")
                {
                }

            }
        }

    }
}