/// <summary>
/// PageExtension ECPPostedSalesShipmenC01 (ID 51001) extends Record Posted Sales Shipment.
/// </summary>
pageextension 51001 "ECPPostedSalesShipmenC01" extends "Posted Sales Shipment"
{

    actions
    {
        addlast(Reporting)
        {
            action(ECPEtiquetasC01)
            {
                ApplicationArea = All;
                trigger OnAction()
                var
                    replEtiquetas: Report Etiqueta;
                    rlSalesShipmentsLine: Record "Sales Shipment Line";
                begin
                    rlSalesShipmentsLine.SetRange("Document No.", Rec."No.");
                    replEtiquetas.SetTableView(rlSalesShipmentsLine);
                    replEtiquetas.RunModal();
                end;
            }
        }        // Add changes to page actions here
    }

    var
        myInt: Integer;
}