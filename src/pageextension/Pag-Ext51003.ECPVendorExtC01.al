pageextension 51003 "ECPVendorExtC01" extends "Vendor Card"
{

    actions
    {
        addlast(processing)
        {
            action(ECPCrearClienteC01)
            {
                ApplicationArea = All;
                Caption = 'Crear Cliente con mismos datos';

                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    cDato: Label 'Cliente';
                begin
                    if rlCustomer.get(Rec."No.") then begin
                        if Confirm('El ' + cDato + ' %1 ya existe como %2,\' +
                                    'Desea actualizar ficha con datos de %3?', true,
                                    rlCustomer."No.", rlCustomer.Name, Rec.Name) then begin
                            rlCustomer.TransferFields(Rec, true, true);
                            rlCustomer.Modify(true);
                            Message(cDato + ' modificado con exito');
                        end;
                    end else begin
                        rlCustomer.init;
                        rlCustomer.TransferFields(Rec, true, true);
                        rlCustomer.Insert(true);
                        // duda: si alguien se ha suscrito a modificaciones unicamente,
                        // este proceso mete datos en la ficha y el suscriptor no se enteraria
                        rlCustomer.TransferFields(Rec, true, true);
                        rlCustomer.modify(true);
                        Message(cDato + ' creado con exito');
                    end;
                end;
            }
        }
    }
    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('Desea salir?', false));
    end;

    procedure RespActionsF(pAction: Action): Boolean;
    begin
        exit(pAction in [Action::LookupOK, Action::OK, Action::Yes]);
    end;
}
