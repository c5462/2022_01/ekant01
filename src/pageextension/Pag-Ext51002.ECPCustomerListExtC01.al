/// <summary>
/// PageExtension "ECPCustomerListExtC01" (ID 51002) extends Record Customer List.
/// </summary>
pageextension 51002 ECPCustomerListExtC01 extends "Customer List"
{
    actions
    {
        addfirst(General)
        {
            action(ECPPaginaFiltrosC01)
            {
                ApplicationArea = All;
                Caption = 'Pagina de filtros';
                trigger OnAction()
                var
                    xlPagFiltros: FilterPageBuilder;
                    rlCustomer: Record Customer;
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    rlPrices: Record "Sales Price";
                    clClien: Label 'Clientes';
                    clMovs: Label 'Movimientos';
                    clPrices: Label 'Precios';
                begin
                    xlPagFiltros.PageCaption('Seleccione los clientes a procesar');
                    xlPagFiltros.AddRecord(clClien, rlCustomer);
                    xlPagFiltros.AddField(clClien, rlCustomer."No.");
                    xlPagFiltros.AddField(clClien, rlCustomer.Name);
                    xlPagFiltros.AddRecord(clMovs, rlCustLedgerEntry);
                    xlPagFiltros.AddField(clMovs, rlCustLedgerEntry."Posting Date");
                    xlPagFiltros.AddRecord(clPrices, rlPrices);
                    xlPagFiltros.AddField(clPrices, rlPrices."Starting Date");
                    if xlPagFiltros.RunModal() then begin
                        rlCustomer.SetView(xlPagFiltros.GetView(clClien, true));
                        if rlCustomer.FindSet(false) then begin
                            repeat
                            // hacer lo que sea previo al proceso
                            until rlCustomer.next() = 0;
                            Message(xlPagFiltros.GetView(clMovs, false));
                            Message(xlPagFiltros.GetView(clPrices, true));
                        end;
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }

            action(ECPSwitchVariableC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Variables globales a BC';
                trigger OnAction()
                var
                    clMensaje: Label 'El valor actual es %1, y lo cambiamos a %2';

                begin
                    Message(clMensaje, xSwitch, not xSwitch);
                    xSwitch := not xSwitch;
                end;
            }
            action(ECPPruebaCapturaErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Codeunit de captura errores';
                trigger OnAction()
                var
                    culECPCaptura: Codeunit ECPCapturarErroresC01;
                begin
                    if culECPCaptura.Run() then begin
                        Message('Se pudo ejecutar la cu');
                    end else begin
                        Message('No se pudo ejecutar la cu, motivo %1', GetLastErrorText());
                    end;
                end;
            }
            action(ECPPrueba2CapturaErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Codeunit de captura errores';
                trigger OnAction()
                var
                    culECPCaptura: Codeunit ECPCapturarErroresC01;
                    i: Integer;
                    xlCodProveedor: Code[20];
                begin
                    xlCodProveedor := '10000';
                    for i := 1 to 5 do begin
                        if culECPCaptura.FuncionParaCapturarErrorF(xlCodProveedor) then begin
                            Message('Se ha creado el proveedor %1', xlCodProveedor);
                        end;
                        xlCodProveedor := IncStr(xlCodProveedor);
                    end;
                end;
            }
        }
        addlast(General)
        {
            action(ECPSwitchVariableGlobalcUnitC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Variables globales a BC con CodeUnit';
                trigger OnAction()
                var
                    clMensaje: Label 'El valor actual es %1, y lo cambiamos a %2';
                begin
                    Message(clMensaje, cuFuncion.GetSwitchF(), not cuFuncion.GetSwitchF());
                    cuFuncion.SetSwitchF(not cuFuncion.GetSwitchF());
                end;
            }
            action(ECPImprimirAlbaranC01)
            {
                trigger OnAction()
                var
                    rSalesInvHeader: Record "Sales Invoice Header";
                    rGenericoVentas: Report ECPGenericoVentasC01;
                begin
                    rSalesInvHeader.SetRange("No.", Rec."No.");
                    //rGenericoVentas.SetTableView();
                end;
            }
        }
        addlast(History)
        {
            action(ECPFacturasYabonosC01)
            {
                Caption = 'Facturas y Abonos';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    FrasYabonosF();
                end;
            }
        }
        addlast(History)
        {
            action(ECPClientesAlmacenGRISC01)
            {
                Caption = 'Clientes Almacen GRIS';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    ClientesAlmGRIS();
                end;
            }
            action(ECPExportarClientesC01)
            {
                Caption = 'Exportar Clientes';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    ExportarClientes();
                end;
            }
            action(ECPSumatorio1C01)
            {
                Caption = 'Sumatorio 1';
                ApplicationArea = All;
                trigger OnAction()
                var
                    rlDetalles: record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetalles.SetRange("Customer No.", Rec."No.");
                    if rlDetalles.FindSet(false) then begin
                        repeat
                            xlTotal += rlDetalles.Amount;
                        until rlDetalles.Next() = 0;
                    end;
                    Message('Total: %1, en %2', xlTotal, CurrentDateTime - xlInicio);
                end;
            }
            action(ECPSumatorio2C01)
            {
                Caption = 'Sumatorio 2';
                ApplicationArea = All;
                trigger OnAction()
                var
                    rlDetalles: record "Detailed Cust. Ledg. Entry";
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetalles.SetRange("Customer No.", Rec."No.");
                    rlDetalles.CalcSums(Amount);   // mas eficiente que el de arriba
                    Message('Total: %1, en %2', rlDetalles.Amount, CurrentDateTime - xlInicio);
                end;
            }
            action(ECPSumatorio3C01)
            {
                Caption = 'Sumatorio 3';
                ApplicationArea = All;
                trigger OnAction()
                var
                    rlCusLed: record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCusLed.SetRange("Customer No.", Rec."No.");
                    rlCusLed.SetLoadFields(Amount);
                    // esto gana rendimiento, pues solo carga en el select el campo que se indica
                    // solo funciona bien si se trata de leer, si hubiera que modificar pierde eficiencia
                    // para esos casos se debe usar otro registro en el que guarde
                    rlCusLed.SetAutoCalcFields(Amount);
                    // esto es lo mas eficiente, evita hacer calcfields en cada iteracion antes de sumar
                    // pero debe estar antes de la primera llamada Find
                    if rlCusLed.FindSet(false) then begin
                        repeat
                            //rlCusLed.CalcFields(Amount);  // esto no es eficiente
                            xlTotal += rlCusLed.Amount;
                        until rlCusLed.Next() = 0;
                    end;
                    Message('Total: %1, en %2', xlTotal, CurrentDateTime - xlInicio);
                end;
            }
            action(ECPSumatorio4C01)
            {
                Caption = 'Sumatorio 4';
                ApplicationArea = All;
                trigger OnAction()
                var
                    rlDetalles: record "Cust. Ledger Entry";
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetalles.SetRange("Customer No.", Rec."No.");
                    rlDetalles.CalcSums(Amount);   // Esto no funciona, desde versiones 2013 en adelante
                    Message('Total: %1, en %2', rlDetalles.Amount, CurrentDateTime - xlInicio);
                end;
            }
            action(ECPExportarExcelC01)
            {
                Caption = 'Exportar Clientes a Excel';
                ApplicationArea = All;
                Image = Excel;
                trigger OnAction()
                var
                    TempLExcel: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xFila: Integer;
                begin
                    TempLExcel.CreateNewBook('Clientes');
                    // Rellenar las celdas de Excel
                    CurrPage.SetSelectionFilter(rlCustomer);
                    rlCustomer.SetAutoCalcFields("Balance (LCY)", "Sales (LCY)");
                    rlCustomer.SetLoadFields("No.", Name, Address, "Responsibility Center", "Balance (LCY)", "Sales (LCY)");
                    if rlCustomer.FindSet(false) then begin
                        xFila := 1;
                        AsignaCeldaF(xFila, 1, rlCustomer.FieldCaption("No."), true, true, TempLExcel);
                        AsignaCeldaF(xFila, 2, rlCustomer.FieldCaption(Name), true, true, TempLExcel);
                        AsignaCeldaF(xFila, 3, rlCustomer.FieldCaption(Address), true, true, TempLExcel);
                        AsignaCeldaF(xFila, 4, rlCustomer.FieldCaption("Responsibility Center"), true, true, TempLExcel);
                        AsignaCeldaF(xFila, 5, rlCustomer.FieldCaption("Balance (LCY)"), true, true, TempLExcel);
                        AsignaCeldaF(xFila, 6, rlCustomer.FieldCaption("Sales (LCY)"), true, true, TempLExcel);
                        repeat
                            xFila += 1;
                            AsignaCeldaF(xFila, 1, rlCustomer."No.", false, false, TempLExcel);
                            AsignaCeldaF(xFila, 2, rlCustomer.Name, false, false, TempLExcel);
                            AsignaCeldaF(xFila, 3, rlCustomer.Address, false, false, TempLExcel);
                            AsignaCeldaF(xFila, 4, rlCustomer."Responsibility Center", false, false, TempLExcel);
                            AsignaCeldaF(xFila, 5, format(rlCustomer."Balance (LCY)"), false, false, TempLExcel);
                            AsignaCeldaF(xFila, 6, format(rlCustomer."Sales (LCY)"), false, false, TempLExcel);
                        until rlCustomer.Next() = 0;
                        TempLExcel.WriteSheet('', '', ''); // esto guarda la hoja
                        TempLExcel.CloseBook();
                        TempLExcel.OpenExcel();
                    end;
                end;
            }
            action(ECPImportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'Importar desde Excel';
                Image = Excel;
                trigger OnAction()
                var
                    TempLExcel: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xFila: Integer;
                    xlFichero: Text;
                    xlInStr: Instream;
                    xlHoja: Text;
                    xlCod: Code[20];
                begin
                    if UploadIntoStream('Seleccione el fichero Excel a importar', '',
                                        'Hojas Excel|*.xls|Libros Excel|*.xlsx',
                                        xlFichero, xlInStr) then begin
                        xlHoja := TempLExcel.SelectSheetsNameStream(xlInStr);
                        if xlHoja = '' then Error('Proceso cancelado');
                        TempLExcel.OpenBookStream(xlInStr, xlHoja);
                        TempLExcel.ReadSheet();
                        //page.Run(page::ECPListaExcelBufferC01, TempLExcel);
                        TempLExcel.SetFilter("Row No.", '>1');
                        if TempLExcel.FindSet(false) then begin
                            repeat
                                case TempLExcel."Column No." of
                                    1:
                                        begin
                                            xlCod := TempLExcel."Cell Value as Text";
                                            if not rlCustomer.get(xlCod) then begin
                                                rlCustomer.Init();
                                                rlCustomer.validate("No.", xlCod);
                                                rlCustomer.Insert(true);
                                            end;
                                        end;
                                    2:
                                        rlCustomer.validate(Name, TempLExcel."Cell Value as Text");
                                    3:
                                        rlCustomer.validate(Address, TempLExcel."Cell Value as Text");
                                    4:
                                        begin
                                            rlCustomer.validate("Responsibility Center", TempLExcel."Cell Value as Text");
                                            rlCustomer.Modify(true);
                                        end;
                                    5:
                                        ; // los campos calculados no se guardan
                                    6:
                                        ; // el sexto campo es calculado, no lo guardo
                                end;
                            until TempLExcel.next() = 0;
                        end;
                    end else begin
                        Error('No se ha podido cargar el fichero');
                    end;
                end;
            }

        }
    }
    local procedure AsignaCeldaF(pFila: Integer; pColum: Integer; pContenido: Text; pNegrita: Boolean; pItalica: Boolean; var TempPExcel: Record "Excel Buffer" temporary)
    var
    begin
        TempPExcel.Init();
        TempPExcel.Validate("Row No.", pFila);
        TempPExcel.Validate("Column No.", pColum);
        TempPExcel.insert(true);  // es de las pocas que al insertar en una temporary se haga un true
        TempPExcel.Validate("Cell Value as Text", pContenido);
        TempPExcel.Validate(Bold, pNegrita);
        TempPExcel.Validate(Italic, pItalica);
        TempPExcel.Modify(true);
    end;

    var
        xSwitch: Boolean;
        cuFuncion: Codeunit "ECPVariablesGlobalesBCC01";

    local procedure FrasYabonosF()
    var
        rlTemp: Record "Sales Invoice Header" temporary;
        rlFras: Record "Sales Invoice Header";
        rlAbon: Record "Sales Cr.Memo Header";
        plFras: Page "Posted Sales Invoices";
    begin
        if rlFras.ReadPermission then begin
            // Recorrer Fras cliente e insertar en tabla Temporal
            rlFras.SetRange("Bill-to Customer No.", Rec."No.");
            //message(rlFras.GetFilter("Bill-to Customer No."));
            if rlFras.FindSet(false, false) then begin
                repeat
                    rlTemp.init;
                    rlTemp.copy(rlFras);
                    rlTemp."No." := 'F-' + rlFras."No.";
                    rlTemp.Insert(false);
                until rlFras.next() = 0;
            end;
            // Recorrer Abonos e insertar en tabla Temporal
            if rlAbon.ReadPermission then begin
                rlAbon.SetView(rlFras.GetView()); // esto aplica los filtros que coinciden, no da error si algun campo no esta en la tabla 2
                //rlAbon.SetRange("Bill-to Customer No.", Rec."No.");
                if rlAbon.FindSet(false, false) then begin
                    repeat
                        rlTemp.init;
                        rlTemp.TransferFields(rlAbon, false);
                        rlTemp."No." := 'A-' + rlAbon."No.";
                        rlTemp.Insert(false);
                    until rlAbon.next() = 0;
                end;
            end;
            // Mostrar tabla temporal
            Page.Run(0, rlTemp);
            //plFras.SetTableView(rlTemp);
            //plFras.SetRecord(rlTemp);
            //plFras.Run();
        end;
    end;

    local procedure ClientesAlmGRIS()
    var
        rlCustomer: Record Customer;
        rlCompany: Record Company;
        i: Integer;
    begin
        // Mostrar la lista de empresas disponibles y de la seleccionada mostrar sus clientes GRIS
        if RespActionsF(page.RunModal(page::Companies, rlCompany)) then begin
            rlCustomer.ChangeCompany(rlCompany.Name);
            i := rlCustomer.FilterGroup();
            rlCustomer.FilterGroup(i + 20);   // cambio el grupo a otra pagina de grupos
            rlCustomer.SetRange("Location Code", 'GRIS');
            rlCustomer.FilterGroup(i);
            // el primer parametro es el num de pagina, si ponemos 0 usa la que la tabla tiene como lookupId
            if RespActionsF(Page.RunModal(page::"Customer List", rlCustomer)) then begin
                Message('Proceso Ok');
                //Rec.SetRecFilters(rlCustomer.GetFilters);
            end else begin
                Error('Proceso cancelado');
            end;
        end else begin
            Error('Proceso cancelado');
        end;
    end;

    procedure RespActionsF(pAction: Action): Boolean;
    begin
        exit(pAction in [Action::LookupOK, Action::OK, Action::Yes]);
    end;

    local procedure ExportarClientes()
    var
        rlCustomer: Record Customer;
        xlInStr: InStream;
        xlOutStr: OutStream;
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;  // es una tabla que no guarda datos
        xlNombreFichero: Text;
        clSeparator: Label '·', Locked = true;
    begin
        TempLConfigTMP.EjemBlob.CreateOutStream(xlOutStr, TextEncoding::Windows);
        rlCustomer := Rec;
        rlCustomer.Find();
        rlCustomer.CalcFields("Balance (LCY)");
        xlOutStr.WriteText(Rec."No." + clSeparator + Rec.Name + clSeparator + format(Rec."Balance (LCY)"));
        xlOutStr.WriteText();
        TempLConfigTMP.EjemBlob.CreateInStream(xlInStr);
        xlNombreFichero := 'Clientes.txt';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            error('No se ha podido descargar el fichero');
        end;
    end;

    procedure GetSelectionFilterF(var rSalida: Record Customer)
    begin
        CurrPage.SetSelectionFilter(rSalida);
    end;
}
