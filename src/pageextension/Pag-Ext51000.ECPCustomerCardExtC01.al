pageextension 51000 "ECPCustomerCardExtC01" extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(ECPVacunasC01)
            {
                Caption = 'Vacunas';

                field(ECPNumVacunasC01; Rec.ECPNumVacunasC01)
                {
                    ToolTip = 'Numero de Vacunas recibidas por el Cliente.';
                    ApplicationArea = All;
                }
                field(ECPFechaUltimaVacunaC01; Rec.ECPFechaUltimaVacunaC01)
                {
                    ToolTip = 'Fecha en la que recibió la ultima Vacuna.';
                    ApplicationArea = All;
                }
                field(ECPTipoVacunaC01; Rec.ECPTipoVacunaC01)
                {
                    ToolTip = 'Tipo de Vacuna, el codigo de la Vacuna.';
                    ApplicationArea = All;
                }
                field(ECPEfectosSecundariosC01; Rec.ECPEfectosSecundariosC01)
                {
                    ToolTip = 'Cosas que le ha pasado al cliente después de recibir la Vacuna.';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(ECPAsignarBloqueadaC01)
            {
                ApplicationArea = All;
                Caption = '💉Asignar vacuna bloqueada';
                //Image

                trigger OnAction()
                begin
                    Rec.validate(ECPTipoVacunaC01, 'MALA');
                end;
            }
            action(ECPCrearProveedorC01)
            {
                ApplicationArea = All;
                Caption = 'Crear proveedor con mismos datos';

                trigger OnAction()
                var
                    rlVendor: Record Vendor;
                    cDato: Label 'Proveedor';
                begin
                    if rlVendor.get(Rec."No.") then begin
                        if Confirm('El ' + cDato + ' %1 ya existe como %2,\' +
                                    'Desea actualizar ficha con datos de %3?', true,
                                    rlVendor."No.", rlVendor.Name, Rec.Name) then begin
                            rlVendor.TransferFields(Rec, true, true);
                            rlVendor.Modify(true);
                            Message(cDato + ' modificado con exito');
                        end;
                    end else begin
                        rlVendor.init;
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.Insert(true);
                        // duda: si alguien se ha suscrito a modificaciones unicamente,
                        // este proceso mete datos en la ficha y el suscriptor no se enteraria
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.modify(true);
                        Message(cDato + ' creado con exito');
                    end;
                end;
            }
            action(ECPExportarExcelC01)
            {
                Caption = 'Exportar Clientes a Excel';
                ApplicationArea = All;
                Image = Excel;
                trigger OnAction()
                var
                    TempLExcel: Record "Excel Buffer" temporary;
                    plLista: Page "Customer List";
                    rlCustomer: Record Customer;
                    xFila: Integer;
                begin
                    TempLExcel.CreateNewBook('Clientes');
                    TempLExcel.WriteSheet('', '', '');
                    // Rellenar las celdas de Excel
                    Message('Seleccionar los clientes a Exportar');
                    plLista.LookupMode(true);
                    plLista.SetRecord(Rec);
                    if plLista.RunModal() in [Action::LookupOK, Action::Yes, Action::OK] then begin
                        // obtenemos el DataSet
                        plLista.GetSelectionFilterF(rlCustomer);
                        if rlCustomer.FindSet(false) then begin
                            AsignaCeldaF(1, 1, rlCustomer.FieldCaption("No."), true, true, TempLExcel);
                            AsignaCeldaF(1, 2, rlCustomer.FieldCaption(Name), true, true, TempLExcel);
                            AsignaCeldaF(1, 3, rlCustomer.FieldCaption(Address), true, true, TempLExcel);
                            AsignaCeldaF(1, 4, rlCustomer.FieldCaption("Balance (LCY)"), true, true, TempLExcel);
                            repeat
                                xFila += 1;
                                AsignaCeldaF(xFila, 1, rlCustomer."No.", false, false, TempLExcel);
                                AsignaCeldaF(xFila, 2, rlCustomer.Name, false, false, TempLExcel);
                                AsignaCeldaF(xFila, 3, rlCustomer.Address, false, false, TempLExcel);
                                AsignaCeldaF(xFila, 4, format(rlCustomer."Balance (LCY)"), false, false, TempLExcel);
                            until rlCustomer.Next() = 0;
                            TempLExcel.CloseBook();
                            TempLExcel.OpenExcel();
                        end;
                    end;
                end;
            }
        }
    }
    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('Desea salir?', false));
    end;

    procedure RespActionsF(pAction: Action): Boolean;
    begin
        exit(pAction in [Action::LookupOK, Action::OK, Action::Yes]);
    end;

    local procedure AsignaCeldaF(pFila: Integer; pColum: Integer; pContenido: Text; pNegrita: Boolean; pItalica: Boolean; var TempPExcel: Record "Excel Buffer" temporary)
    var
    begin
        TempPExcel.Init();
        TempPExcel.Validate("Row No.", pFila);
        TempPExcel.Validate("Column No.", pColum);
        TempPExcel.insert(true);  // es de las pocas que al insertar en una temporary se haga un true
        TempPExcel.Validate("Cell Value as Text", pContenido);
        TempPExcel.Validate(Bold, pNegrita);
        TempPExcel.Validate(Italic, pItalica);
        TempPExcel.Modify(true);
    end;
}
