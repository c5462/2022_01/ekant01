pageextension 51004 "ECPSalesOrderListExtC01" extends "Sales Order List"
{
    actions
    {
        addlast(processing)
        {
            action(ECPExportarPedidosVentaC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar Pedidos Venta a fichero csv';
                Promoted = true;
                trigger OnAction()
                begin
                    ExportarPedidosVentaF();
                end;
            }
            action(ECPImportarPedidosVentaC01)
            {
                ApplicationArea = All;
                Caption = 'Importar Pedidos Venta desde Fichero csv';
                Promoted = true;
                trigger OnAction()
                begin
                    ImportarPedidosVentaF();
                end;
            }
        }
    }

    local procedure ExportarPedidosVentaF()
    var
        rlCabVenta: Record "Sales Header";
        rlLinVenta: Record "Sales Line";
        xlInStr: InStream;
        xlOutStr: OutStream;
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;  // es una tabla que no guarda datos
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        // obtenemos el DataSet
        CurrPage.SetSelectionFilter(rlCabVenta);
        rlCabVenta.SetAutoCalcFields("Amount Including VAT", Amount);
        //rlCabVenta.SetLoadFields("Document Type","No.","Sell-to Customer No.");  
        // poner los campos que intervienen para ganar velocidad
        if rlCabVenta.FindSet(false) then begin
            repeat
                xlOutStr.WriteText('C' + cSeparador +
                                    format(rlCabVenta."Document Type") + cSeparador +
                                    rlCabVenta."No." + cSeparador +
                                    rlCabVenta."Sell-to Customer No." + cSeparador +
                                    rlCabVenta."Sell-to Customer Name" + cSeparador +
                                    rlCabVenta."Sell-to Address" + cSeparador +
                                    rlCabVenta."Sell-to City" + cSeparador +
                                    rlCabVenta."Sell-to County" + cSeparador +
                                    rlCabVenta."Sell-to Post Code" + cSeparador +
                                    rlCabVenta."Sell-to Country/Region Code" + cSeparador +
                                    rlCabVenta."Bill-to Customer No." + cSeparador +
                                    rlCabVenta."Bill-to Name" + cSeparador +
                                    rlCabVenta."Bill-to Address" + cSeparador +
                                    rlCabVenta."Bill-to City" + cSeparador +
                                    rlCabVenta."Bill-to County" + cSeparador +
                                    rlCabVenta."Bill-to Post Code" + cSeparador +
                                    rlCabVenta."Bill-to Country/Region Code" + cSeparador +
                                    rlCabVenta."External Document No." + cSeparador +
                                    format(rlCabVenta.Status, 0, 9) + cSeparador +
                                    format(rlCabVenta."Posting Date", 0, 9) + cSeparador +
                                    format(rlCabVenta."Order Date", 0, 9) + cSeparador +
                                    format(rlCabVenta.Amount, 0, 9) + cSeparador +
                                    format(rlCabVenta."Amount Including VAT", 0, 9));
                xlOutStr.WriteText();
                rlLinVenta.SetRange("Document Type", rlCabVenta."Document Type");
                rlLinVenta.SetRange("Document No.", rlCabVenta."No.");
                if rlLinVenta.FindSet(false) then begin
                    repeat
                        // a la hora de exportar a fichero usar el FORMAT con 0,9 para datos standarizados
                        xlOutStr.WriteText('L' + cSeparador +
                                    StrSubstNo('%1', rlLinVenta."Document Type") + cSeparador +
                                    rlLinVenta."Document No." + cSeparador +
                                    format(rlLinVenta."Line No.", 0, 9) + cSeparador +
                                    format(rlLinVenta.Type, 0, 9) + cSeparador +
                                    rlLinVenta."No." + cSeparador +
                                    rlLinVenta.Description + cSeparador +
                                    rlLinVenta."Description 2" + cSeparador +
                                    rlLinVenta."Unit of Measure" + cSeparador +
                                    Format(rlLinVenta.Quantity, 0, 9) + cSeparador +
                                    format(rlLinVenta."Unit Price", 0, 9) + cSeparador +
                                    format(rlLinVenta."Unit Cost", 0, 9) + cSeparador +
                                    format(rlLinVenta."VAT %", 0, 9) + cSeparador +
                                    format(rlLinVenta."Line Discount %", 0, 9) + cSeparador +
                                    format(rlLinVenta."Line Discount Amount", 0, 9) + cSeparador +
                                    format(rlLinVenta."Line Amount", 0, 9) + cSeparador +
                                    format(rlLinVenta."Amount", 0, 9) + cSeparador +
                                    format(rlLinVenta."Amount Including VAT", 0, 9) + cSeparador +
                                    rlLinVenta."Gen. Bus. Posting Group" + cSeparador +
                                    rlLinVenta."Gen. Prod. Posting Group" + cSeparador +
                                    rlLinVenta."Posting Group");
                        xlOutStr.WriteText();
                    until rlLinVenta.next = 0;
                end;
            until rlCabVenta.next = 0;
            TempLConfigTMP.EjemBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
            TempLConfigTMP.EjemBlob.CreateInStream(xlInStr);
            xlNombreFichero := 'PedidosVenta.csv';
            if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
                error('No se ha podido descargar el fichero');
            end;
        end;
    end;

    local procedure ImportarPedidosVentaF()
    var
        rlSalesHead: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        rlLogError: Record ECPLogC01;
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;  // es una tabla que no guarda datos
        xlInStr: InStream;
        xlLinea: Text;
        cuErrores: Codeunit ECPCapturarErroresC01;
        clNew: Label 'P', Locked = true;
        i: Integer;
        xlNumIncid: Integer;
        xCodigo: Code[20];
        xlLin: Integer;
        xCodLin: Code[20];
        xlHayErr: Boolean;
        xlMsj: Text;
    begin
        TempLConfigTMP.EjemBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('Seleccionar el fichero csv a Importar|*.csv', xlInStr) then begin
            while not xlInStr.EOS do begin
                xlInStr.ReadText(xlLinea);
                case true of
                    xlLinea.ToUpper() [1] = 'C':    // por si pudiera venir en minusculas
                        begin
                            xlHayErr := false;
                            rlSalesHead.init;
                            Evaluate(rlSalesHead."Document Type", xlLinea.Split(cSeparador).Get(2));
                            xCodigo := clNew + xlLinea.Split(cSeparador).Get(3);
                            rlSalesHead."No." := xCodigo;
                            if rlSalesHead.Insert(true) then begin
                                for i := 4 to 4 do begin   //23
                                    if not cuErrores.ValidarCabPedidos(rlSalesHead, xlLinea, i, cSeparador) then begin
                                        // guardar en Log lista de incidencias
                                        rlLogError.GuardaLogF(
                                            StrSubstNo('Incidencia Cab Ped %1, campo %2: ',
                                            xCodigo, i) + GetLastErrorText());
                                        xlNumIncid += 1;
                                    end;
                                end;
                                rlSalesHead.modify(true);
                            end else begin
                                // guardar en Log lista de incidencias
                                rlLogError.GuardaLogF(
                                    StrSubstNo('Error al añadir Pedido %1: ', xCodigo) +
                                    GetLastErrorText());
                                xlNumIncid += 1;
                                xlHayErr := true;
                            end;
                        end;
                    xlLinea[1] = 'L':
                        begin
                            if not xlHayErr then begin
                                // no procesa lineas de pedido cuya cabecera no se ha guardado
                                rlSalesLine.Init();
                                Evaluate(rlSalesLine."Document Type", xlLinea.Split(cSeparador).Get(2));
                                xCodLin := clNew + xlLinea.Split(cSeparador).Get(3);
                                rlSalesLine.validate("Document No.", xCodLin);
                                Evaluate(rlSalesLine."Line No.", xlLinea.Split(cSeparador).Get(4));
                            end;
                            if rlSalesLine.Insert(true) then begin
                                for i := 5 to 6 do begin  //21
                                    if not cuErrores.ValidarLinPedidos(rlSalesLine, xlLinea, i, cSeparador) then begin
                                        // guardar en Log lista de incidencias
                                        rlLogError.GuardaLogF(
                                            StrSubstNo('Incidencia Ped %1, Lin %2, campo %3: ',
                                            xCodLin, xlLin, i) +
                                            GetLastErrorText());
                                        xlNumIncid += 1;
                                    end;
                                end;
                                rlSalesLine.modify(true);
                            end else begin
                                // guardar en Log lista de incidencias
                                rlLogError.GuardaLogF(
                                    StrSubstNo('Error al añadir Lin %2, Pedido %1: ',
                                    xCodigo, xlLin) +
                                    GetLastErrorText());
                                xlNumIncid += 1;
                            end;
                        end;
                end;
            end;
        end else begin
            Error('Cancelado, No hay fichero a importar');
        end;
        Message('Fin del proceso de importacion, Incidencias %1', xlNumIncid);
        if xlNumIncid > 1 then begin
            Page.Run(page::ECPListaLogsC01, rlLogError);
        end;
    end;

    var
        cSeparador: Label '·', Locked = true;
}
