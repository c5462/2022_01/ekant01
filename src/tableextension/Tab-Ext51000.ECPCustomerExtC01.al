tableextension 51000 "ECPCustomerExtC01" extends Customer
{
    Caption = 'Extension de Clientes';
    Description = 'Es la tabla de Clientes Extendida';

    fields
    {
        field(50100; ECPNumVacunasC01; Integer)
        {
            Caption = 'Numero de Vacunas';
            FieldClass = FlowField;
            CalcFormula = count(ECPLinPlanVacunacionC01 where(ClienteAvacunar = field("No.")));
            Editable = false;
        }
        field(50110; ECPFechaUltimaVacunaC01; Date)
        {
            Caption = 'Fecha Ultima Vacuna';
            FieldClass = FlowField;
            CalcFormula = max(ECPLinPlanVacunacionC01.FechaVacunacion where(ClienteAvacunar = field("No.")));
            Editable = false;
        }
        field(50115; ECPFechaSigteVacunaC01; Date)
        {
            Caption = 'Fecha Siguiente Vacuna';
            FieldClass = FlowField;
            CalcFormula = min(ECPLinPlanVacunacionC01.FechaSigteDosis where(ClienteAvacunar = FIELD("No."), FechaSigteDosis = FILTER('>0D')));
            //CalcFormula = min(ECPLinPlanVacunacionC01.FechaVacunacion where(ClienteAvacunar = field("No."),));
            Editable = false;
        }
        field(50120; ECPTipoVacunaC01; Code[10])
        {
            Caption = 'Tipo de Vacuna';
            DataClassification = CustomerContent;
            TableRelation = ECPVariosC01.Codigo where(Tipo = const(Vacuna));
            trigger OnValidate()
            var
                rlVarios: Record ECPVariosC01;
            begin
                if rlVarios.get(rlVarios.Tipo::Vacuna, Rec.ECPTipoVacunaC01) then begin
                    rlVarios.TestField(Bloqueado, false);
                    // if rlVarios.Bloqueado then
                    //     Error('No se puede usar este tipo');
                end;
            end;
        }
        field(50130; ECPEfectosSecundariosC01; Text[100])
        {
            Caption = 'Efectos Secundarios';
            DataClassification = CustomerContent;
        }
        modify(Name)    // Suscripcion a cuando se modifica el campo de la tabla
        {
            trigger OnAfterValidate()
            var
                culFunciones: Codeunit ECPVariablesGlobalesBCC01;
            begin
                culFunciones.NombreF(Rec.Name);
            end;
        }
    }
    trigger OnAfterInsert()
    begin
        SincronizarClientesF(xAction::insert);
    end;

    trigger OnAfterModify()

    begin
        SincronizarClientesF(xAction::Modify);
    end;

    trigger OnAfterDelete()
    begin
        SincronizarClientesF(xAction::insert);
    end;

    local procedure SincronizarClientesF(pAccion: Option)
    var
        rlCompany: Record Company;
        rlCustomNew: Record Customer;
    begin
        rlCompany.setfilter(Name, '<>%1', CompanyName);
        if rlcompany.findset(false, false) then begin
            repeat
                if rlCustomNew.ChangeCompany(rlCompany.Name) then begin
                    rlCustomNew.Init();
                    rlCustomNew.copy(Rec);
                    case pAccion of
                        xAction::Delete:
                            rlCustomNew.delete(false);
                        xAction::modify:
                            rlCustomNew.Modify(false);
                        xAction::Insert:
                            rlCustomNew.Insert(false);
                    end;
                end;
            until rlCompany.next() = 0;
        end;
    end;

    var
        xAction: Option Insert,Modify,Delete;
}
