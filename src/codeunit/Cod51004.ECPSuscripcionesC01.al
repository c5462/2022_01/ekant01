codeunit 51004 "ECPSuscripcionesC01"
{
    SingleInstance = true;
    // importante, para mejorar el rendimiento

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnAfterPostSalesDoc(var SalesHeader: Record "Sales Header")
    var
        culECPFuncionesAppC01: Codeunit ECPFuncionesAppC01;
    begin
        culECPFuncionesAppC01.EjercicioF(SalesHeader);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]

    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    // el nombre de la procedire debe ser como lo de arriba pero sin cars especiales
    // y OJO, no se puede cambiar el parametro q da el ctrl+space
    begin
        SalesHeader.TestField("External Document No.");
    end;

    [EventSubscriber(ObjectType::Table, database::"Record Link", 'OnAfterValidateEvent', 'URL1', false, false)]
    local procedure DatabaseRecordLinkOnAfterValidateEventURL1(CurrFieldNo: Integer; var Rec: Record "Record Link"; var xRec: Record "Record Link")
    begin

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostOnAfterPostPurchaseDoc(PurchInvHdrNo: Code[20])
    var
        culECPFuncionesAppC01: Codeunit ECPFuncionesAppC01;
    begin
        // en este apartado de suscripciones interesa que el codigo sea lo mas pequeño posible
        // por eso se saca de aqui y se hace fuera
        culECPFuncionesAppC01.MensajeFraCompraF(PurchInvHdrNo);
    end;

    [EventSubscriber(ObjectType::Table, Database::ECPLinPlanVacunacionC01, 'OnAfterCalcularFechaProxVacunaF', '', false, false)]
    local procedure DatabaseECPLinPlanVacunacionC01(var IsHandled: Boolean)
    begin
        IsHandled := true;
        // en los afterget se indica si quieres que a la vuelta siga el proceso del original o no
        // 
    end;
}