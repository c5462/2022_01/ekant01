codeunit 51005 "ECPFuncionesAppC01"
{
    procedure MensajeFraCompraF(PurchInvHdrNo: Code[20])
    begin
        if PurchInvHdrNo <> '' then begin
            Message('El usuario %1 ha creado la factura %2', UserId, PurchInvHdrNo);
        end;
    end;

    procedure FuncionEjemploF(pCodCliente: Code[20]; pParam2: Integer; var prCustomer: Record Customer) xSalida: Text
    var
        i: Integer;
    begin

    end;

    procedure EjemploVariablesEntSalidaF(var pTexto: Text)
    begin
        pTexto := 'Texto de salida';
    end;

    procedure EjemploVariablesEntSalida2F(pTexto: TextBuilder)
    begin
        pTexto.Append('Texto de salida');
    end;

    procedure EjemploArrayF()
    var
        mlNumeros: Array[20] of Text;
        i: Integer;
        x: Integer;
        xlNumElem: Integer;
        xlNum: Text;
        xEsta: Boolean;
    begin
        repeat
            x += 1;
            randomize(x);
            xlNumElem := ArrayLen(mlNumeros);
            for i := 1 to xlNumElem do begin
                mlNumeros[i] := format(i);
            end;
            //
            repeat
                i := Random(xlNumElem);
                if mlNumeros[i] <> '' then begin
                    Message('Bola numero: %1', mlNumeros[i]);
                    mlNumeros[i] := '';
                    xlNumElem := CompressArray(mlNumeros);
                    // elimina casillas en blanco
                end;

            until xlNumElem = 0;
        until Confirm('Jugar de nuevo ?', true) = false;
    end;
    /// <summary>
    /// Devuelve el ultimo numero de Linea de un Pedido, Oferta, ... que se pase en pRec
    /// </summary>
    /// <param name="pRec">Record "Sales Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimoNumLineaF(pRec: Record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line no.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure UltimoNumLineaF(pRec: Record "Purchase Line"): Integer
    var
        rlRec: Record "Purchase Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line no.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;


    procedure UltimoNumLineaF(pRec: Record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line no.");
        rlRec.SetRange("Journal Template Name", pRec."Journal Batch Name");
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure EjemploSobreCargaFuncionesF(p1: Integer; p2: Decimal; var p3: Text; p4: Boolean): Decimal
    begin
        if p4 = true then begin
            p2 := p1 * 12.45;
            p3 := 'Funciones nuevas';
        end else begin
            p2 := p1 + 100;
            p3 := 'Funciones viejas';
        end;
        exit(p2);
    end;

    procedure EjemploSobreCargaFuncionesF(p1: Integer; p2: Decimal; var p3: Text): Decimal
    begin
        exit(EjemploSobreCargaFuncionesF(p1, p2, p3, false));
    end;

    procedure SintaxisF()
    var
        cSeparador: Label '.', Locked = true; // locked evita que se traduzca y cambien esta cadena
        i: Integer;
        xlTexto: Text;
        x: Decimal;
        xBool: Boolean;
        xValorPorDefecto: Boolean;
        xParametro1: Text;
        xOpcionPorDefecto: Integer;
        Ventana: Dialog;
        xlFecha: Date;
        xlInStr: Instream;
    begin
        i := 1;
        i := i + 1;
        i += 1;
        // operadores
        i := Round(x + i, 0.5); // redondea a multiplos de 0.5, esta da un error porque espera un integer
        i := Round(x + i, 2); // redondea a numeros pares, el
        i := Round(x + i, 1, '>');   // redondea al mas cercano
        i := 9 div 2; // saca la parte entera de dividir entre 2, seria el 4
        i := 9 mod 2; // saca el resto de la division, o sea 1
        i := abs(-1); // devuelve un 1
        xBool := not (true); // devuelve false
        xBool := false and true; // devuelve false
        xBool := false or true; // devuelve true
        xBool := false xor true; // devuelve true si cualquiera de los dos es true, pero no si son ambos
        // oper. relacionales    // un xor es como un interruptor de la luz conmutado
        xBool := 1 < 2;
        xBool := 1 > 2;
        xBool := 1 <= 2;
        xBool := 1 >= 2;
        xBool := 1 <> 2;
        xBool := 1 = 2;
        xBool := 1 in [1, 'B', 3, 'y'];
        xBool := 'a' in ['a' .. 'z'];
        // funciones muy usadas
        Message('Proceso terminado');
        Message('El cliente %1 no tiene deudas', xlTexto);
        Message('Estamos en %1%2%1%1', 2, 0);  // devuelve Estamos en 2022
        Error('Proceso cancelado por %1', UserId);
        Error(''); // esto da un error sin mensaje y hace un roll-back
        xBool := Confirm('Preguntamos %1', xValorPorDefecto, xParametro1);
        i := StrMenu('Enviar,Factura,Enviar y Facturar', xOpcionPorDefecto, 'Titulo del cuadro');
        case i of
            0:
                Error('No seleccionado nada, ha pulsado Esc o Cancelar');
            1:
                Message('Seleccionado Enviar');
            2:
                Message('Seleccionado Facturar');
        end;
        // OJO, antes de abrir una venta hay que preguntar:
        if GuiAllowed then begin
            Ventana.Open('Procesando bucle i #1######\' +
                         'Procesando bucle x #2######');
            //Ventana.Open('Proceso @1@@@@@@@');    // esto no funciona bien en web, sale un %
        end;
        for i := 1 to 10 do begin
            if GuiAllowed then Ventana.Update(1, i);
            for x := 1 to 20 do begin
                if GuiAllowed then Ventana.Update(2, x);
            end;
        end;
        if GuiAllowed then Ventana.close;

        // concatenacion textos
        xlTexto := 'Emilio ';
        xlTexto := xlTexto + 'Cantero ';
        xlTexto += 'Pardo';
        xlTexto := StrSubstNo('%1 %2 %3', 'Emilio', 'Cantero', 'Pardo');

        // trabajar con cadenas de texto
        i := MaxStrLen(xlTexto);
        i := MaxStrLen('ABC');
        xlTexto := CopyStr('ABC', 2, 1); // devuelve B
        xlTexto := CopyStr('ABC', 2); // devuelve BC
        xlTexto := xlTexto.Substring(2); // es lo mismo que el de arriba, pero tratando el Txt como objeto
        i := StrPos('Elefante', 'e');  // devuelve un 3
        i := xlTexto.IndexOf('e', 4);  // busca la e a partir de la 4 posicion
        xlTexto := UpperCase(xlTexto);
        xlTexto := xlTexto.ToUpper();  // lo mismo que el de arriba
        xlTexto := LowerCase(xlTexto);
        xlTexto := xlTexto.ToLower();  // lo mismo que el de arriba
        xlTexto := xlTexto.TrimEnd('\') + '\'; // esto no estaba, se asegura que hay un solo \ al final
        xlTexto := '123,456,Tecon,s.l.,789';
        Message(SelectStr(2, xlTexto)); // devuelve 456
        xlTexto := '123,456,Tecon servicios, s.l.,789';
        Message(SelectStr(4, xlTexto)); // devuelve s.l.
        Message(SelectStr(5, xlTexto)); // 789
        xlTexto := '123·456·Tecon servicios, s.l.·789';  // mejor usar este separador, encima del 3
        Message(xlTexto.Split('·').Get(1)); // devuelve 123
        Message(xlTexto.Split('·').Get(2)); // devuelve 456
        Message(xlTexto.Split('·').Get(3)); // devuelve Tecon...
        Message(xlTexto.Split('·').Get(4)); // devuelve 789
        // en lugar de '.' mejor usamos un label
        Message(xlTexto.Split(cSeparador).Get(4)); // devuelve 789, ver def.vble en var
        xlTexto := '21-05-2022';
        Evaluate(xlFecha, ConvertStr(xlTexto, '-', '\'));
        Evaluate(xlFecha, xlTexto.Replace('-', '/').Replace('.', '\'));
        xlTexto := 'Elefante';
        Message(DelChr(xlTexto, '<', 'e')); // elimina la e primera, devuelve Elfante
        Message(DelChr(xlTexto, '>', 'e')); // elimina la e ultima, devuelve Elefant
        Message(DelChr(xlTexto, '=', 'e')); // elimina la e primera, devuelve Elfante
        Message(DelChr(xlTexto, '<=>', 'e')); // elimina la e en todo, devuelve Elfant
        i := Power(3, 2);  // eleva 3 al cuadrado, devuelve 9
        i := Power(2, 3);  // eleva 2 al cubo, devuelve 8
        xlTexto := UserId;
        xlTexto := CompanyName;
        Today;
        WorkDate();
        Time;

        // Gestion de Ficheros con Streams
        // tenemos dos tipos InStream que saca info del stream y OutStream que trae info de fuera
        // el OutStream necesita un objeto BLOB o un File para depositar alli algo
        // xlInStr.ReadText(); es el que mas se usa
        // Creamos el contenedor, ver Funcion StreamsF()
    end;

    procedure StreamsF()
    var
        xlInStr: InStream;
        xlOutStr: OutStream;
        TempLConfigTMP: Record ECPConfiguracionC01 temporary;  // es una tabla que no guarda datos
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        TempLConfigTMP.EjemBlob.CreateOutStream(xlOutStr); // pincha la pajita
        xlOutStr.WriteText('textoprueba'); // escribe a traves de la pajita
        TempLConfigTMP.EjemBlob.CreateInStream(xlInStr); // pincha la pajita a traves de la vble
        xlInStr.ReadText(xlLinea);  // lee y carga a traves de la pajita
        xlNombreFichero := 'c:\Pedido.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            error('No se ha podido descargar el fichero');
        end;
    end;

    procedure LeftF(pCadena: text; pNumCars: Integer): Text
    begin
        if strlen(pCadena) > pNumCars then
            exit(copystr(pCadena, 1, pNumCars))
        else
            exit(pCadena);
    end;

    procedure RightF(pCadena: Text; pNumCars: Integer): Text
    begin
        if strlen(pCadena) > pNumCars then
            exit(CopyStr(pCadena, (strlen(pCadena) - pNumCars + 1)))
        else
            exit(pCadena);
    end;

    procedure Leer()
    var
        rlCustomer: Record Customer;
        rlLinPedido: Record "Sales Line";
        rlDimen: Record "Dimension Set Entry";
        rlDiario: Record "Gen. Journal Line";
        rlCompany: Record "Company Information";
    begin
        //
        if rlCustomer.get('10000') then begin

        end;
        //
        if rlLinPedido.Get(rlLinPedido."Document Type"::Order, '101005', 10000) then begin

        end;
        if rlDimen.get(7, 'GRUPONEGOCIO') then begin

        end;
        //
        if rlDiario.get() then begin

        end;
        //
        if rlCompany.get() then begin

        end;

    end;
    // otras funciones
    local procedure OtrasFuncionesF()
    var
        rlCabVenta: Record "Sales Header";
        rlCabFras: Record "Sales Invoice Header";
    begin
        rlCabFras.TransferFields(rlCabVenta);
    end;

    local procedure UsoDeListaF()
    var
        xLista: List of [Text];
    begin
        //xLista.Add();
    end;

    procedure AbrirListaClientesF(pNotif: Notification)
    var
        rlCustomer: record Customer;
        clDato: Label 'CodigoCliente';
    begin
        if pNotif.HasData(clDato) then begin
            rlCustomer.SetRange("No.", pNotif.GetData(clDato));
        end;
        page.Run(page::"Customer List", rlCustomer);
    end;

    procedure AbrirFichaClientesF(pNotif: Notification)
    var
        rlCustomer: record Customer;
        clDato: Label 'CodigoCliente';
    begin
        if pNotif.HasData(clDato) then begin
            if not rlCustomer.get(pNotif.GetData(clDato)) then begin
                Error('No existe el cliente %1', pNotif.GetData(clDato));
            end;
        end;
        page.Run(page::"Customer Card", rlCustomer);
    end;

    procedure EjercicioF(pSalesHeader: Record "Sales Header")
    var
        rlCustomer: Record Customer;
        rlLinVentas: Record "Sales Line";
        xlNot: Notification;
    begin
        if rlCustomer.get(pSalesHeader."Sell-to Customer No.") then begin
            if rlCustomer.ECPTipoVacunaC01 = '' then begin
                xlNot.Id(CreateGuid());
                xlNot.Message('El cliente no tiene vacunas');
                xlNot.AddAction('Abrir Ficha clientes', Codeunit::ECPFuncionesAppC01, 'AbrirFichaClientesF');
                xlNot.SetData('CodigoCliente', rlCustomer."No.");
                xlNot.Send();
            end;
            rlLinVentas.SetRange("Document No.", pSalesHeader."No.");
            rlLinVentas.SetRange("Document Type", pSalesHeader."Document Type");
            //rlLinVentas.Setfilter(Amount, '<%1', 100);
            rlLinVentas.SetLoadFields("Document No.", "Document Type", "Line No.", Amount);
            if rlLinVentas.FindSet(false) then begin
                repeat
                    Clear(xlNot);
                    xlNot.Id(CreateGuid());
                    xlNot.Message(StrSubstNo('La linea %1 de %3 num %2 es inferior a 100', rlLinVentas."Line No.", rlLinVentas."Document No.", rlLinVentas."Document Type"));
                    xlNot.Send();
                until rlLinVentas.Next() = 0;
            end;
        end;
    end;
}