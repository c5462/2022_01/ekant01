/// <summary>
/// Codeunit ECPInstallC01 (ID 51000).
/// </summary>
codeunit 51000 "ECPInstallAppC01"
{
    Subtype = Install;   // en funcion de este tipo cambian los triggers
    trigger OnInstallAppPerDatabase()
    begin
        // aqui se ponen aquellos procesos que se hacen al instalar el programa para las dBases Comunes
    end;

    trigger OnInstallAppPerCompany()
    begin
        // aqui se ponen aquellos procesos que se hacen al instalar el programa para las dBases por cada Empresa
    end;

}