/// <summary>
/// Codeunit ECPUpgradesC01 (ID 51001).
/// </summary>
codeunit 51001 "ECPUpgradesAppC01"
{
    Subtype = Upgrade;   // en funcion de este tipo cambian los triggers
    trigger OnCheckPreconditionsPerCompany()
    begin
        // aqui se ponen aquellos procesos que se hacen antes de actualizar app para las dBases Comunes
    end;

    trigger OnCheckPreconditionsPerDatabase()
    begin
        // aqui se ponen aquellos procesos que se hacen anes de instalar el programa para las dBases por cada Empresa
    end;

    trigger OnUpgradePerCompany()
    begin
        // aqui se ponen aquellos procesos de actualizacion propiamente dichos para las dBases Comunes
    end;

    trigger OnUpgradePerDatabase()
    begin
        // aqui se ponen aquellos procesos de actualizacion propiamente dichos para las dBases por cada Empresa
    end;

}