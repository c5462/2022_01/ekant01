codeunit 51003 "ECPCapturarErroresC01"
{
    trigger OnRun()
    var
        rlCliente: Record Customer;
        pCodigo: Label 'AAA', Locked = true, Comment = 'msj para programadores';
    // el locked es para que no aparezca en las traducciones
    begin
        if not rlCliente.get(pCodigo) then begin
            rlCliente.Init();
            rlCliente.Validate("No.", pCodigo);
            rlCliente.Insert(true);      // importante hacer el insert antes por las suscripciones
            rlCliente.validate(Name, 'Proveedor de prueba');
            rlCliente.Validate(Address, 'Calle del proveedor');
            rlCliente."VAT Registration No." := 'xxx'; // para que no verifique el NIf, porque puede dar errores
            rlCliente.Validate("Payment Method Code", 'naymenos');
            rlCliente.Modify(true);
        end;
        Message('Funcion ejecutada correctamente');
    end;

    procedure FuncionParaCapturarErrorF(pCodigo: Code[20]): Boolean
    var
        rlVendor: Record Vendor;
    begin
        if not rlVendor.get(pCodigo) then begin
            rlVendor.Init();
            rlVendor.Validate("No.", pCodigo);
            rlVendor.Insert(true);      // importante hacer el insert antes por las suscripciones
            if AsignarDatosProveedorF(rlVendor) then begin // esta funcion si permite el TrayFunction 
                rlVendor.Modify(true);
            end else begin
                Message('Error en asignar datos (%1)', GetLastErrorText());
            end;
            exit(true);
        end;
        Message('Ya existe el proveedor %1', pcodigo);
    end;

    [TryFunction]
    local procedure AsignarDatosProveedorF(var rlVendor: Record Vendor)
    begin
        rlVendor.validate(Name, 'Proveedor de prueba');
        rlVendor.Validate(Address, 'Calle del proveedor');
        rlVendor."VAT Registration No." := 'xxx'; // para que no verifique el NIf, porque puede dar errores
        rlVendor.Validate("Payment Method Code", 'naymenos');
    end;

    [TryFunction]
    procedure ValidarCabPedidos(var pSalesHeader: Record "Sales Header"; pLinea: Text; pGet: Integer; pSeparator: Text)
    var
        xlFecha: Date;
        xlNum: Decimal;
        xlStatus: Enum "Sales Document Status";
    begin
        case pGet of
            4:
                pSalesHeader.Validate("Sell-to Customer No.", pLinea.Split(pSeparator).Get(pGet));
            5:
                pSalesHeader.Validate("Sell-to Customer Name", pLinea.Split(pSeparator).Get(pGet));
            6:
                pSalesHeader.Validate("Sell-to Address", pLinea.Split(pSeparator).Get(pGet));
            7:
                pSalesHeader.Validate("Sell-to City", pLinea.Split(pSeparator).Get(pGet));
            8:
                pSalesHeader.Validate("Sell-to County", pLinea.Split(pSeparator).Get(pGet));
            9:
                pSalesHeader.Validate("Sell-to Post Code", pLinea.Split(pSeparator).Get(pGet));
            10:
                pSalesHeader.Validate("Sell-to Country/Region Code", pLinea.Split(pSeparator).Get(pGet));
            11:
                pSalesHeader.Validate("Bill-to Customer No.", pLinea.Split(pSeparator).Get(pGet));
            12:
                pSalesHeader.Validate("Bill-to Name", pLinea.Split(pSeparator).Get(pGet));
            13:
                pSalesHeader.Validate("Bill-to Address", pLinea.Split(pSeparator).Get(pGet));
            14:
                pSalesHeader.Validate("Bill-to City", pLinea.Split(pSeparator).Get(pGet));
            15:
                pSalesHeader.Validate("Bill-to County", pLinea.Split(pSeparator).Get(pGet));
            16:
                pSalesHeader.Validate("Bill-to Post Code", pLinea.Split(pSeparator).Get(pGet));
            17:
                pSalesHeader.Validate("Bill-to Country/Region Code", pLinea.Split(pSeparator).Get(pGet));
            18:
                pSalesHeader.Validate("External Document No.", pLinea.Split(pSeparator).Get(pGet));
            19:
                if Evaluate(xlStatus, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesHeader.Validate(Status, xlStatus);
                end;
            20:
                if Evaluate(xlFecha, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesHeader.Validate("Posting Date", xlFecha);
                end;
            21:
                if Evaluate(xlFecha, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesHeader."Order Date" := xlFecha;
                end;
            22:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesHeader.Validate(Amount, xlNum);
                end;
            23:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesHeader.Validate("Amount Including VAT", xlNum);
                end;
        end;
    end;

    [TryFunction]
    procedure ValidarLinPedidos(var pSalesLine: Record "Sales Line"; pLinea: Text; pGet: Integer; pSeparator: Text)
    var
        xlTipoLin: Enum "Sales Line Type";
        xlNum: Decimal;
        xlFecha: date;
    begin
        case pGet of
            5:
                if Evaluate(xlTipoLin, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate(Type, xlTipoLin);
                end;
            6:
                pSalesLine.validate("No.", pLinea.Split(pSeparator).Get(pGet));
            7:
                pSalesLine.validate(Description, pLinea.Split(pSeparator).Get(pGet));
            8:
                pSalesLine.validate("Description 2", pLinea.Split(pSeparator).Get(pGet));
            9:
                pSalesLine.validate("Unit of Measure Code", pLinea.Split(pSeparator).Get(pGet));
            10:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate(Quantity, xlNum);
                end;
            11:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate("Unit Price", xlNum);
                end;
            12:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate("Unit Cost", xlNum);
                end;
            13:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate("VAT %", xlNum);
                end;
            14:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate("Line Discount %", xlNum);
                end;
            15:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate("Line Discount Amount", xlNum);
                end;
            16:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate("Line Amount", xlNum);
                end;
            17:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate(Amount, xlNum);
                end;
            18:
                if Evaluate(xlNum, pLinea.Split(pSeparator).Get(pGet)) then begin
                    pSalesLine.validate("Amount Including VAT", xlNum);
                end;
            19:
                pSalesLine.validate("Gen. Bus. Posting Group", pLinea.Split(pSeparator).Get(pGet));
            20:
                pSalesLine.validate("Gen. Prod. Posting Group", pLinea.Split(pSeparator).Get(pGet));
            21:
                pSalesLine.validate("Posting Group", pLinea.Split(pSeparator).Get(pGet));
        end;
    end;
}