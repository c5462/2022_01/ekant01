/// <summary>
/// Codeunit ECPFuncionesC01 (ID 51002).
/// </summary>
codeunit 51002 "ECPVariablesGlobalesBCC01"
{
    SingleInstance = true;   // hace que las vbles globales se mantengan entre objetos
    /// <summary>
    /// Guarda un valor de forma global a todas la Apps, se mantiene hasta que cierras BC
    /// </summary>
    /// <param name="pValor">Boolean.</param>
    procedure SetSwitchF(pValor: Boolean)
    begin
        xSwitch := pValor;
    end;
    /// <summary>
    /// Devuelve el valor guardado
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetSwitchF(): Boolean
    begin
        exit(xSwitch);
    end;
    /// <summary>
    /// Guarda nombre en Vble Global para luego hacer un Get
    /// </summary>
    /// <param name="pName">Text.</param>
    procedure NombreF(pName: Text)
    begin
        xNombre := pName;
    end;
    /// <summary>
    /// Devuelve Nombre guardado desde el Set
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreF(): Text
    begin
        exit(xNombre);
    end;

    var
        xSwitch: Boolean;
        xNombre: Text;
}