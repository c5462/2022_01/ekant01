codeunit 51006 "ECPCodeunitLentaC01"
{
    trigger OnRun()
    begin
        EjecutarTareaF();
    end;

    local procedure EjecutarTareaF()
    begin
        GuardaLog('Tarea iniciada');
        //Commit();
        //
        Sleep(10000);
        //
        GuardaLog('Tarea terminada');
    end;

    local procedure GuardaLog(pTex: Text)
    var
        rlLog: Record ECPLogC01;
    begin
        rlLog.Init();
        rlLog.Insert(true);
        rlLog.validate(Mensaje, pTex + ': ' + Format(Time));
        rlLog.Modify(true);
    end;
}