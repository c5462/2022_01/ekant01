/// <summary>
/// Report Id. Documentos de Ventas
/// </summary>
report 51003 "ECPMAADocumentoVentasC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'DocumentoVentas.rdlc';

    dataset
    {
        dataitem(SalesHeader; "Sales Header") //Cabeceras
        {
            RequestFilterFields = "No.", "Document Type";

            column(No_SalesHeader; "No.")
            {
            }
            column(PostingDate_SalesHeader; "Posting Date")
            {
            }
            column(SelltoCustomerNo_SalesHeader; "Sell-to Customer No.")
            {
            }
            column(EtiquetaDoc; xDocumentLabel)
            {
            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }
            column(xCustAddr3; xCustAddr[3])
            {

            }
            column(xCustAddr4; xCustAddr[4])
            {

            }
            column(xCustAddr5; xCustAddr[5])
            {

            }
            column(xCustAddr6; xCustAddr[6])
            {

            }
            column(xCustAddr7; xCustAddr[7])
            {

            }
            column(xCustAddr8; xCustAddr[8])
            {

            }
            column(xCompanyAddr1; xCompanyAddr[1])
            {

            }
            column(xCompanyAddr2; xCompanyAddr[2])
            {

            }
            column(xCompanyAddr3; xCompanyAddr[3])
            {

            }
            column(xCompanyAddr4; xCompanyAddr[4])
            {

            }
            column(xCompanyAddr5; xCompanyAddr[5])
            {

            }
            column(xCompanyAddr6; xCompanyAddr[6])
            {

            }
            column(xCompanyAddr7; xCompanyAddr[7])
            {

            }
            column(xCompanyAddr8; xCompanyAddr[8])
            {

            }
            column(Logo; rCompanyInfo.Picture)
            {

            }
            dataitem("Sales Line"; "Sales Line") //Lineas
            {
                DataItemLink = "Document No." = field("No."), "Document Type" = field("Document Type");

                column(No_SalesLine; "No.")
                {
                }
                column(Description_SalesLine; Description)
                {
                }
                column(Quantity_SalesLine; Quantity)
                {
                }
                column(UnitPrice_SalesLine; "Unit Price")
                {
                }
                column(LineDiscount_SalesLine; "Line Discount %")
                {
                }
                column(Amount_SalesLine; Amount)
                {
                }
            }
            //Despues de obtener el registro vemos que hacer en cada opcion
            trigger OnAfterGetRecord()
            begin
                case SalesHeader."Document Type" of
                    SalesHeader."Document Type"::Quote:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Oferta';
                        end;
                    SalesHeader."Document Type"::Order:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Pedido';
                        end;
                    SalesHeader."Document Type"::Invoice:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Factura';
                        end;
                end;
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(TipoDoc; rsalesHeader."Document Type") //Ayuda para tipo de documento
                    {
                        Caption = 'Tipo de Documento';
                        ApplicationArea = All;
                    }
                    field(NumDoc; xDocumentNo)
                    {
                        ApplicationArea = All;
                        Caption = 'Número de Documento';

                        trigger OnLookup(var Txt: Text): Boolean
                        var
                            plSalesHeader: Page "Sales List";
                        begin
                            plSalesHeader.SetTableView(rSalesHeader); //Aplico filtros de tabla a página
                            plSalesHeader.LookupMode := true; //Pongo la página en modo lookup

                            //Ejecuto la página y controlo la elección del usuario
                            if plSalesHeader.RunModal() = Action::LookupOK then begin
                                plSalesHeader.SetSelectionFilter(rSalesHeader); //Aplico la selección del usuario de la página a la tabla, ahora vemos si tienen registros
                                if rSalesHeader.FindSet() then begin
                                    repeat
                                        xDocumentNo += rSalesHeader."No." + '|'; //Elaboramos el filtro
                                    until rSalesHeader.Next() = 0;
                                end;
                                xDocumentNo := DelChr(xDocumentNo, '>', '|'); //Borramos el último caracter
                            end;
                        end;
                    }
                }
            }
        }
    }
    //Triggers globales del informe
    trigger OnPreReport()

    begin
        rCompanyInfo.Get(); //Cargamos la tabla de info. de empresa
        rCompanyInfo.CalcFields(Picture);
        cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);
    end;
    //Variables globales
    var
        rSalesHeader: Record "Sales Header";
        rCompanyInfo: Record "Company Information";
        cuFormatAddress: Codeunit "Format Address";
        xDocumentLabel: Text;
        xDocumentNo: Text;
        xCustAddr: array[8] of Text;
        xCompanyAddr: array[8] of Text;
}