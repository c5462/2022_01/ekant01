/// <summary>
/// Report Listado Proyectos.
/// </summary>
report 51001 "ECPListadoProyectosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'ListadoProyectos.rdlc';
    DefaultLayout = RDLC;
    Caption = 'Listado de Proyectos';

    dataset
    {
        dataitem(Proyectos; Job)
        {
            RequestFilterFields = "No.", "Creation Date", Status;
            DataItemTableView = sorting("No.");
            column(NumProyecto; "No.")
            {
            }
            column(NumCliente; "Bill-to Customer No.")
            {
            }
            column(Descripcion; Description)
            {
            }
            column(FechaCreacion; "Creation Date")
            {
            }
            column(NombreCliente; "Bill-to Name")
            {
            }
            column(Bloqueado; Blocked)
            {
            }
            column(Estado; Status)
            {
            }
            column(FechaInicio; "Starting Date")
            {
            }
            column(Color; xColor)
            {
            }

            dataitem(Tareas; "Job Task")
            {

                RequestFilterFields = "Job Task No.", "Job Task Type";
                DataItemLink = "Job No." = field("No.");

                column(JobNo_Tareas; "Job Task No.")
                {
                }
                column(JobTaskType_Tareas; "Job Task Type")
                {
                }
                column(Description_Tareas; Description)
                {
                }
                column(StartDate_Tareas; "Start Date")
                {
                }
                column(EndDate_Tareas; "End Date")
                {
                }

                dataitem(LineasPlanif; "Job Planning Line")
                {
                    DataItemLink = "Job No." = field("Job No."), "Job Task No." = field("Job Task No.");
                    RequestFilterFields = "No.", "Planning Date", Type;

                    column(No_LineasPlanif; "No.")
                    {
                    }
                    column(LineNo_LineasPlanif; "Line No.")
                    {
                    }

                    column(Type_LineasPlanif; "Type")
                    {
                    }
                    column(Description_LineasPlanif; Description)
                    {
                    }
                    column(Quantity_LineasPlanif; Quantity)
                    {
                    }
                    column(PlanningDate_LineasPlanif; "Planning Date")
                    {
                    }
                    column(LineAmount_LineasPlanif; "Line Amount")
                    {
                    }

                    dataitem("Job Ledger Entry"; "Job Ledger Entry")
                    {
                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");

                        column(DocumentNo_JobLedgerEntry; "Document No.")
                        {
                        }
                        column(DocumentDate_JobLedgerEntry; "Document Date")
                        {
                        }
                        column(LineType_JobLedgerEntry; "Line Type")
                        {
                        }
                        column(Type_JobLedgerEntry; "Type")
                        {
                        }
                        column(EntryType_JobLedgerEntry; "Entry Type")
                        {
                        }
                        column(LedgerEntryType_JobLedgerEntry; "Ledger Entry Type")
                        {
                        }
                    }
                }
            }
            trigger OnAfterGetRecord()
            begin
                case Proyectos.Status of
                    Proyectos.Status::Completed:
                        begin
                            xColor := 'Green'
                        end;
                    Proyectos.Status::Open:
                        begin
                            xColor := 'Blue'
                        end;
                    Proyectos.Status::Planning:
                        begin
                            xColor := 'Red'
                        end;
                    Proyectos.Status::Quote:
                        begin
                            xColor := 'Yellow'
                        end;
                end;
            end;
        }


    }


    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }
    var
        xColor: Text[10];
}