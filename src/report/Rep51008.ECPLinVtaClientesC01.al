/// <summary>
/// Informe de Ventas a Clientes con sus Cobros.
/// </summary>
report 51008 "ECPLinVtaClientesC01"
{
    ApplicationArea = All;
    Caption = 'Ventas a Clientes con Cobros';
    UsageCategory = Lists;
    DefaultLayout = RDLC;
    RDLCLayout = 'VentasClientesConCobros.rdlc';

    dataset
    {
        dataitem(Cliente; "Customer")
        {
            RequestFilterFields = "No.";
            column(No_Cliente; "No.")
            {
            }
            column(Name_Cliente; Name)
            {
            }
            column(Address_Cliente; Address)
            {
            }
            column(City_Cliente; City)
            {
            }
            column(County_Cliente; County)
            {
            }

            dataitem(SalesInvLine; "Sales Invoice Line")
            {
                DataItemLink = "Sell-to Customer No." = field("No.");
                DataItemTableView = sorting("Sell-to Customer No.", "No.");
                RequestFilterFields = "Posting Date";
                column(DocumentNo_SalesInvLine; "Document No.")
                {
                }
                column(No_SalesInvLine; "No.")
                {
                }
                column(Type_SalesInvLine; "Type")
                {
                }
                column(Description_SalesInvLine; Description)
                {
                }
                column(Quantity_SalesInvLine; Quantity)
                {
                }
                column(UnitPrice_SalesInvLine; "Unit Price")
                {
                }
                column(AmountIncludingVAT_SalesInvLine; "Amount Including VAT")
                {
                }
                dataitem(Cobros; "Cust. Ledger Entry")
                {
                    DataItemLink = "Document No." = field("Document No.");
                    DataItemTableView = sorting("Posting date");
                    RequestFilterFields = "Posting Date";
                    column(DocumentNo_Cobros; "Document No.")
                    {
                    }
                    column(PostingDate_Cobros; "Posting Date")
                    {
                    }

                    column(Description_Cobros; Description)
                    {
                    }
                    column(Amount; Amount)
                    {

                    }
                    column(RemainingAmount_Cobros; "Remaining Amount")
                    {
                    }

                    trigger OnPreDataItem()
                    begin
                        if xFechaCobros <> '' then begin
                            Cobros.Setfilter("Posting Date", xFechaCobros);
                        end;
                    end;
                }
                trigger OnPreDataItem()
                begin
                    if xFechaFras <> '' then begin
                        SalesInvLine.Setfilter("Posting Date", xFechaFras);
                    end;
                    //SalesInvLine.SetRange("Sell-to Customer No.", Cliente."No.");
                end;
            }

        }
    }
    requestpage
    {
        layout
        {
            area(content)
            {
                group(MargenFechas)
                {
                    Caption = 'Margenes para el Listado';


                }
            }
        }

    }
    trigger OnPreReport()
    begin
        rCompanyInfo.Get();
        rCompanyInfo.CalcFields(Picture);
    end;

    var
        rCompanyInfo: Record "Company Information";
        xFechaFras: Text;
        xFechaCobros: Text;
}
