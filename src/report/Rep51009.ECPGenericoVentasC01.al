report 51009 "ECPGenericoVentasC01"
{
    Caption = 'Report Generico de Ventas';
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'GenericoVentas.rdlc';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem(SalesInvoiceHeader; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";

            dataitem(SalesInvoiceLine; "Sales Invoice Line")
            {
                DataItemLink = "Document No." = field("No.");

                trigger OnAfterGetRecord()
                begin
                    ECPLinGenericaVentasC01.Init();
                    ECPLinGenericaVentasC01.TransferFields(SalesInvoiceLine);
                    ECPLinGenericaVentasC01.Insert(false);
                end;

            }
            trigger OnPreDataItem()
            begin
                if SalesInvoiceHeader.GetFilters() = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            var
            begin
                ECPCabeceraGenericaVentasC01.Init();
                ECPCabeceraGenericaVentasC01.TransferFields(SalesInvoiceHeader);
                ECPCabeceraGenericaVentasC01.Insert(false);
                xCaptionInforme := 'Factura';
            end;
        }
        dataitem(SalesShipmentHeader; "Sales Shipment Header")
        {
            RequestFilterFields = "No.";
            dataitem(SalesShipmentLine; "Sales Shipment Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                begin
                    ECPLinGenericaVentasC01.Init();
                    ECPLinGenericaVentasC01.TransferFields(SalesShipmentLine);
                    ECPLinGenericaVentasC01.Insert(false);
                end;
            }
            trigger OnPreDataItem()
            begin
                if SalesShipmentHeader.GetFilters() = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                ECPCabeceraGenericaVentasC01.Init();
                ECPCabeceraGenericaVentasC01.TransferFields(SalesShipmentHeader);
                ECPCabeceraGenericaVentasC01.Insert(false);
                xCaptionInforme := 'Albarán';
            end;
        }
        dataitem(ECPCabeceraGenericaVentasC01; ECPCabeceraGenericaVentasC01)
        {
            DataItemTableView = sorting("Document Type", "No.");
            column(No_ECPCabeceraGenericaVentasC01; "No.")
            {
            }
            column(SelltoCustomerNo_ECPCabeceraGenericaVentasC01; "Sell-to Customer No.")
            {
            }
            column(SelltoCustomerName_ECPCabeceraGenericaVentasC01; "Sell-to Customer Name")
            {
            }
            column(PostingDate_ECPCabeceraGenericaVentasC01; "Posting Date")
            {
            }
            column(xCaptionInforme; xCaptionInforme)
            {

            }

            dataitem(ECPLinGenericaVentasC01; ECPLinGenericaVentasC01)
            {
                DataItemLink = "Document No." = field("No.");
                column(No_ECPLinGenericaVentasC01; "No.")
                {
                }
                column(Description_ECPLinGenericaVentasC01; Description)
                {
                }
                column(Quantity_ECPLinGenericaVentasC01; Quantity)
                {
                }

                trigger OnAfterGetRecord()
                begin

                end;

            }
            trigger OnAfterGetRecord()
            begin

            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }
    }
    var
        xCaptionInforme: Text;

}