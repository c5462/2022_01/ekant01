report 51002 "ECPFacturaVentaC01"
{
    Caption = 'Factura Venta';
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'Factura Venta.rdlc';

    dataset
    {
        dataitem(SalesInvoiceHeader; "Sales Invoice Header")
        {
            column(No_SalesInvHeader; "No.")
            {

            }
            column(xCustAdd1_SalesInHeader; xCustAddr[1])
            {

            }
            column(xCustAdd2_SalesInHeader; xCustAddr[2])
            {

            }
            column(xCustAdd3_SalesInHeader; xCustAddr[3])
            {

            }
            column(xCustAdd4_SalesInHeader; xCustAddr[4])
            {

            }
            column(xCustAdd5_SalesInHeader; xCustAddr[5])
            {

            }
            column(xCustAdd6_SalesInHeader; xCustAddr[6])
            {

            }
            column(xCustAdd7_SalesInHeader; xCustAddr[7])
            {

            }
            column(xCustAdd8_SalesInHeader; xCustAddr[8])
            {

            }
            dataitem(Integer; Integer)
            {
                column(Number; Number)
                {
                }
                dataitem(SalesInvoiceLine; "Sales Invoice Line")
                {
                    // DataItemLink = "Document No." = field("No.");
                    column(LineNo_SalesInvLine; "Line No.")
                    {
                    }
                    trigger OnPreDataItem()
                    begin
                        SalesInvoiceLine.SetRange("Document No.", SalesInvoiceHeader."No.");
                    end;
                }
                trigger OnPreDataItem()
                begin
                    Integer.SetRange(Number, 1, xCopias);
                end;
            }
            trigger OnAfterGetRecord()
            begin
                cuFormatAdress.SalesInvSellTo(xCustAddr, SalesInvoiceHeader);
                cuFormatAdress.SalesInvShipTo(xShipAddr, xCustAddr, SalesInvoiceHeader);
                rCompanyInfo.get();
                cuFormatAdress.Company(xCompanyAddr, rCompanyInfo);
            end;
        }

    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(xCopias; xCopias)
                    {
                        ApplicationArea = All;
                        Caption = 'Nº de Copias';
                    }
                }
            }
        }
    }
    var
        xCustAddr: array[8] of Text;
        xShipAddr: array[8] of Text;
        xCompanyAddr: array[8] of Text;
        rCompanyInfo: Record "Company Information";
        cuFormatAdress: Codeunit "Format Address";
        xCopias: Integer;
}