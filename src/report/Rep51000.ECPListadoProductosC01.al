/// <summary>
/// Listado de Productos, primer contacto con 
/// </summary>
report 51000 "ECPListadoProductosC01"

{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'ListadoProductos.rdlc';
    AdditionalSearchTerms = 'Listado Productos Ventas-Compras';
    Caption = 'Listado Productos Ventas-Compras';

    dataset
    {
        dataitem(Productos; Item)
        {
            RequestFilterFields = "No.", "Item Category Code";
            column(No_Productos; "No.")
            {

            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }
            dataitem(LinFraVta; "Sales Invoice Line")
            {
                DataItemLink = "No." = field("No.");
                RequestFilterFields = "Posting Date";
                column(Quantity_FraVentas; Quantity)
                {
                }
                column(UnitPrice_FraVentas; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FraVentas; "Amount Including VAT")
                {
                }
                column(PostingDate_LinFraVta; "Posting Date")
                {
                }
                column(NomCliente; rCustomer.Name)
                {

                }
                column(CodCliente; rCustomer."No.")
                {

                }
                column(DocNum_LinFraVta; LinFraVta."Document No.")
                {

                }
                trigger OnAfterGetRecord()
                begin
                    if not rCustomer.get(LinFraVta."Sell-to Customer No.") then begin
                        Clear(rCustomer);
                    end;
                    //     rCustomer.SetRange("No.", LinFraVta."Sell-to Customer No.");
                    //     rCustomer.FindFirst();
                end;

            }
            dataitem(LinFraComp; "Purch. Inv. Line")
            {
                DataItemLink = "No." = field("No.");

                column(UnitCost_LinFraComp; "Unit Cost")
                {
                }
                column(Quantity_LinFraComp; Quantity)
                {
                }
                column(AmountIncludingVAT_LinFraComp; "Amount Including VAT")
                {
                }
                column(PostingDate_LinFraComp; "Posting Date")
                {
                }

                column(NomProveedor; rVendor.Name)
                {

                }
                column(CodProveedor; rVendor."No.")
                {

                }
                column(DocNum_LinFraComp; "Document No.")
                {
                }

                trigger OnPreDataItem()
                begin
                    LinFraComp.SetRange("No.", Productos."No.");
                end;

                trigger OnAfterGetRecord()
                begin
                    if not rVendor.get(LinFraComp."Buy-from Vendor No.") then begin
                        Clear(rVendor);
                    end;
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        //     actions
        //     {
        //         area(processing)
        //         {
        //             action(ActionName)
        //             {
        //                 ApplicationArea = All;

        //             }
        //         }
        //     }
    }

    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;

}