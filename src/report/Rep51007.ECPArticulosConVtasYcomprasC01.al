report 51007 "ECPArticulosConVtasYcomprasC01"
{
    ApplicationArea = All;
    Caption = 'Articulos Con Ventas y Compras';
    UsageCategory = Lists;
    DefaultLayout = RDLC;
    RDLCLayout = 'VentasYcomprasArticulos.rdlc';

    dataset
    {
        dataitem(Cliente; "Customer")
        {
            column(No_Cliente; "No.")
            {
            }
            column(Name_Cliente; Name)
            {
            }
            column(Address_Cliente; Address)
            {
            }
            column(City_Cliente; City)
            {
            }
            column(County_Cliente; County)
            {
            }

            dataitem(SalesInvLine; "Sales Invoice Line")
            {
                DataItemLink = "Sell-to Customer No." = field("No.");
                DataItemTableView = sorting("Sell-to Customer No.", "No.");

                column(DocumentNo_SalesInvLine; "Document No.")
                {
                }
                column(No_SalesInvLine; "No.")
                {
                }
                column(Type_SalesInvLine; "Type")
                {
                }
                column(Quantity_SalesInvLine; Quantity)
                {
                }
                column(UnitPrice_SalesInvLine; "Unit Price")
                {
                }
                column(AmountIncludingVAT_SalesInvLine; "Amount Including VAT")
                {
                }
                dataitem("Extended Text Line"; "Extended Text Line")
                {
                    DataItemLink = "No." = field("No.");
                    column(Text_ExtendedTextLine; "Text")
                    {

                    }
                }
                trigger OnPreDataItem()
                begin
                    SalesInvLine.SetRange(Type, rSalesLine.Type);
                    SalesInvLine.SetRange("Sell-to Customer No.", Cliente."No.");
                end;
            }

        }
    }
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                    field(TipoDoc; rsalesLine.Type) //Ayuda para tipo de documento
                    {
                        Caption = 'Tipo de Documento';
                        ApplicationArea = All;
                        trigger OnValidate()
                        begin
                            xTipo := rSalesLine.Type;
                        end;
                    }
                }
            }
        }
        actions
        {
            area(processing)
            {
            }
        }
    }
    trigger OnPreReport()
    begin
        rCompanyInfo.Get();
        rCompanyInfo.CalcFields(Picture);
    end;

    var
        rSalesLine: Record "Sales Invoice Line";
        rCompanyInfo: Record "Company Information";
        xTipo: Integer;
}
