/// <summary>
/// Report Documento de Venta.
/// </summary>
report 51006 "ECPDocumentoVentaWordC01"
{
    UsageCategory = Administration;
    Caption = 'Documento de Venta Word';
    ApplicationArea = All;
    DefaultLayout = Word;
    WordLayout = 'DocumentoVentas.docx';

    dataset
    {
        dataitem(SalesHeader; "Sales Header") //Cabeceras
        {
            RequestFilterFields = "No.", "Document Type";

            column(No_SalesHeader; "No.")
            {
            }
            column(PostingDate_SalesHeader; "Posting Date")
            {
            }
            column(SelltoCustomerNo_SalesHeader; "Sell-to Customer No.")
            {
            }
            column(EtiquetaDoc; xDocumentLabel)
            {
            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }
            column(xCustAddr3; xCustAddr[3])
            {

            }
            column(xCustAddr4; xCustAddr[4])
            {

            }
            column(xCustAddr5; xCustAddr[5])
            {

            }
            column(xCustAddr6; xCustAddr[6])
            {

            }
            column(xCustAddr7; xCustAddr[7])
            {

            }
            column(xCustAddr8; xCustAddr[8])
            {

            }
            column(xCompanyAddr1; xCompanyAddr[1])
            {

            }
            column(xCompanyAddr2; xCompanyAddr[2])
            {

            }
            column(xCompanyAddr3; xCompanyAddr[3])
            {

            }
            column(xCompanyAddr4; xCompanyAddr[4])
            {

            }
            column(xCompanyAddr5; xCompanyAddr[5])
            {

            }
            column(xCompanyAddr6; xCompanyAddr[6])
            {

            }
            column(xCompanyAddr7; xCompanyAddr[7])
            {

            }
            column(xCompanyAddr8; xCompanyAddr[8])
            {

            }
            column(Logo; rCompanyInfo.Picture)
            {

            }
            column(xTotalIVA11; xTotalIVA[1, 1])
            {

            }
            column(xTotalIVA21; xTotalIVA[2, 1])
            {

            }
            column(xTotalIVA31; xTotalIVA[3, 1])
            {

            }
            column(xTotalIVA41; xTotalIVA[4, 1])
            {

            }
            column(xTotalIVA12; xTotalIVA[1, 2])
            {

            }
            column(xTotalIVA22; xTotalIVA[2, 2])
            {

            }
            column(xTotalIVA32; xTotalIVA[3, 2])
            {

            }
            column(xTotalIVA42; xTotalIVA[4, 2])
            {

            }
            column(xTotalIVA13; xTotalIVA[1, 3])
            {

            }
            column(xTotalIVA23; xTotalIVA[2, 3])
            {

            }
            column(xTotalIVA33; xTotalIVA[3, 3])
            {

            }
            column(xTotalIVA43; xTotalIVA[4, 3])
            {

            }

            dataitem(Copias; Integer)
            {
                column(CopiasItem; Number)
                {

                }
                dataitem("Sales Line"; "Sales Line") //Lineas
                {
                    //DataItemLink = "Document No." = field("No."), "Document Type" = field("Document Type");
                    column(No_SalesLine; "No.")
                    {
                    }
                    column(Description_SalesLine; Description)
                    {
                    }
                    column(Quantity_SalesLine; Quantity)
                    {
                    }
                    column(UnitPrice_SalesLine; "Unit Price")
                    {
                    }
                    column(LineDiscount_SalesLine; "Line Discount %")
                    {
                    }
                    column(Amount_SalesLine; Amount)
                    {
                    }
                    column(LineNo_SalesLine; "Line No.")
                    {
                    }
                    dataitem("Extended Text Line"; "Extended Text Line")
                    {
                        DataItemLink = "No." = field("No.");
                        column(Text_ExtendedTextLine; "Text")
                        {

                        }
                    }
                    dataitem("Item Cross Reference"; "Item Cross Reference")
                    {
                        DataItemLink = "Item No." = field("No.");
                        DataItemTableView = where("Cross-Reference Type" = const(Customer));
                        column(CrossReferenceNo_ItemCrossReference; "Cross-Reference No.")
                        {
                        }
                        column(Description_ItemCrossReference; Description)
                        {
                        }
                    }
                    trigger OnPreDataItem()
                    begin
                        "Sales Line".SetRange("Document No.", SalesHeader."No.");
                        "Sales Line".SetRange("Document Type", SalesHeader."Document Type");
                    end;
                }
                trigger OnPreDataItem()
                begin
                    Copias.SetRange(Number, 0, xCopias);
                end;
            }
            //Despues de obtener el registro vemos que hacer en cada opcion
            trigger OnAfterGetRecord()
            begin
                case SalesHeader."Document Type" of
                    SalesHeader."Document Type"::Quote:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Oferta';
                        end;
                    SalesHeader."Document Type"::Order:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Pedido';
                        end;
                    SalesHeader."Document Type"::Invoice:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Factura';
                        end;
                end;
                CalcularIVA();
            end;

            trigger OnPreDataItem()
            begin
                SalesHeader.SetRange("Document Type", rSalesHeader."Document Type");
                SalesHeader.SetFilter("No.", xDocumentNo);
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(TipoDoc; rsalesHeader."Document Type") //Ayuda para tipo de documento
                    {
                        Caption = 'Tipo de Documento';
                        ApplicationArea = All;
                    }
                    field(NumDoc; xDocumentNo)
                    {
                        ApplicationArea = All;
                        Caption = 'Número de Documento';

                        trigger OnLookup(var Txt: Text): Boolean
                        var
                            plSalesHeader: Page "Sales List";
                        begin
                            rSalesHeader.SetRange("Document Type", rSalesHeader."Document Type");
                            plSalesHeader.SetTableView(rSalesHeader); //Aplico filtros de tabla a página
                            plSalesHeader.LookupMode := true; //Pongo la página en modo lookup

                            //Ejecuto la página y controlo la elección del usuario
                            if plSalesHeader.RunModal() = Action::LookupOK then begin
                                plSalesHeader.SetSelectionFilter(rSalesHeader); //Aplico la selección del usuario de la página a la tabla, ahora vemos si tienen registros
                                if rSalesHeader.FindSet() then begin
                                    repeat
                                        xDocumentNo += rSalesHeader."No." + '|'; //Elaboramos el filtro
                                    until rSalesHeader.Next() = 0;
                                end;
                                xDocumentNo := DelChr(xDocumentNo, '>', '|'); //Borramos el último caracter
                            end;
                        end;
                    }
                    field(xCopias; xCopias)
                    {
                        Caption = 'Copias Docum, ademas del original';
                        ApplicationArea = All;
                        trigger OnValidate()
                        begin
                            if xCopias < 0 then xCopias := 0;
                        end;
                    }

                }
            }
        }
    }
    //Triggers globales del informe
    trigger OnPreReport()

    begin
        rCompanyInfo.Get(); //Cargamos la tabla de info. de empresa
        rCompanyInfo.CalcFields(Picture);
        cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);
    end;

    procedure CalcularIVA()
    var
        rlSalesLine: Record "Sales Line";
        rlTempVATamount: Record "VAT Amount Line" temporary;
        i: Integer;
    begin
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        if rlSalesLine.FindSet() then begin
            repeat
                rlTempVATamount.SetRange("VAT Identifier", rlSalesLine."VAT Identifier");
                if rlTempVATamount.FindFirst() then begin
                    rlTempVATamount."VAT Base" += rlSalesLine.Amount;
                    rlTempVATamount."VAT Amount" += rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATamount."Amount Including VAT" += rlSalesLine."Amount Including VAT";
                    rlTempVATamount.Modify(false);
                end else begin
                    rlTempVATamount.Init();
                    rlTempVATamount."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVATamount."VAT Base" := rlSalesLine.Amount;
                    rlTempVATamount."VAT %" := rlSalesLine."VAT %";
                    rlTempVATamount."VAT Amount" := rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATamount."Amount Including VAT" := rlSalesLine."Amount Including VAT";
                    rlTempVATamount.Insert(false);
                end;
            until rlSalesLine.next() = 0;
        end;
        Clear(xTotalIVA);
        if rlTempVATamount.FindFirst() then
            repeat
                i += 1;
                //for i := 1 to 4 do begin
                xTotalIVA[1, i] := Format(rlTempVATamount."VAT Base");
                xTotalIVA[2, i] := Format(rlTempVATamount."VAT %");
                xTotalIVA[3, i] := Format(rlTempVATamount."VAT Amount");
                xTotalIVA[4, i] := Format(rlTempVATamount."Amount Including VAT");
            //end;
            until rlTempVATamount.Next() = 0;
    end;

    //Variables globales
    var
        rSalesHeader: Record "Sales Header";
        rCompanyInfo: Record "Company Information";
        cuFormatAddress: Codeunit "Format Address";
        xDocumentLabel: Text;
        xDocumentNo: Text;
        xCustAddr: array[8] of Text;
        xCompanyAddr: array[8] of Text;
        xCopias: Integer;
        xTotalIVA: array[4, 4] of Text;
}