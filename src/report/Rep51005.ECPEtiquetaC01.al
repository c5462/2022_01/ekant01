/// <summary>
/// Report "ECP"ECP"ECP"ECP"ECP"ECPEtiquetaC01"C01"C01"C01"C01"C01" (ID 51005).
/// </summary>
report 51005 Etiqueta
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'Etiqueta.rdlc';
    //UseRequestPage = false;

    dataset
    {
        dataitem("Sales Shipment Line"; "Sales Shipment Line")
        {
            column(DocumentNo_SalesShipmentLine; "Document No.")
            {
            }
            column(No_SalesShipmentLine; "No.")
            {
            }
            column(Description_SalesShipmentLine; Description)
            {
            }
            dataitem(Copias; Integer)
            {
                column(Number; Number)
                {

                }
                trigger OnPreDataItem()
                begin
                    Copias.SetRange(Number, 1, "Sales Shipment Line".Quantity);
                end;
            }
        }
    }
}